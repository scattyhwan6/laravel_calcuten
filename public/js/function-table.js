$(document).ready(function () {
    $(".txtMult, .txtBahan,.txtAlat,.txtKalkulasi input").keyup(multInputs);
    function multInputs() {
        var mult = 0;
        var bhn = 0;
        var alat = 0;
        var totalD = 0;
        var disc = 0;
        var satTotal = 0;
        var kal = 0;
        //tenaga kerja
        $("tr.txtMult").each(function () {
            // get the values from this row:
            var $val1 = $('.val1', this).val();
            var $val2 = $('.val2', this).val();
            var $total = ($val1 * 1) * ($val2 * 1)
            $('.multTotal', this).text($total);
            mult += $total;
        });
        $("#grandTotal").text(mult);



        //bahan
        $("tr.txtBahan").each(function () {
            // get the values from this row:
            var $bhnKoef = $('.bhnKoef', this).val();
            var $bhnPrice = $('.bhnPrice', this).val();
            var $total = ($bhnKoef * 1) * ($bhnPrice * 1)
            $('.bahanTotal', this).text($total);
            bhn += $total;
        });
        $("#grandTotal_Bahan").text(bhn);

        //peralatan
        $("tr.txtAlat").each(function () {
            // get the values from this row:
            var $alatKoef = $('.alatKoef', this).val();
            var $alatPrice = $('.alatPrice', this).val();
            var $total = ($alatKoef * 1) * ($alatPrice * 1)
            $('.alatTotal', this).text($total);
            alat += $total;
        });
        $("#grandTotal_alat").text(alat);

        var $value = (mult + alat + bhn)
        totalD += $value;
        $("#totalAll").text(totalD);


        var disctxt = $('#chDiscount').val();
        var dec = (disctxt / 100).toFixed(2);
        var $discont = (totalD * dec)
        disc += $discont;
        $("#result").text(disc);

        var sat = (disc + totalD)
        satTotal += sat
        $("#hargaresult").text(satTotal);



    }
    $('#btnAdd').click(function () {
        var count = 1, first_row = $('#Row2');
        while (count-- > 0)
            first_row.clone().appendTo('#kalkulasi-table');
    });
});



function demoFromHTML() {
    var pdf = new jsPDF('p', 'pt', 'letter', true);

    source = $('#sumberdaya_table')[0];

    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 922,

    };

    pdf.fromHTML(
        source, // HTML string or DOM elem ref.
        margins.left, // x coord
        margins.top, {// y coord
            'width': margins.width, // max width of content on PDF
            'elementHandlers': specialElementHandlers
        },
        function (dispose) {

            pdf.save('calculaten.pdf');
        }
        , margins);
}
