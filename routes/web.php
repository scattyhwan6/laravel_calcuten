<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('home');
});
Route::get('/', function () {
    return view('home');
});
Route::get('/login', function () {
    return view('login');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/register', function () {
    return view('register');
});
Route::get('/filefolder', function () {
    return view('filefolder');
});
Route::get('/membership', function () {
    return view('membership');
});
Route::get('/pembayaran', function () {
    return view('pembayaran');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/contactus', function () {
    return view('contactus');
});
Route::get('/tester', function () {
    return view('tester');
});
Route::get('/grouprequest', function () {
    return view('grouprequest');
});
Route::get('/validation', function () {
    return view('validation');
});
Route::get('/newmember', function () {
    return view('newmember');
});
Route::get('/ciptakarya', function () {
    return view('ciptakarya');
});
Route::get('/subciptakarya', function () {
    return view('subciptakarya');
});
Route::get('/kalkulasi', function () {
    return view('kalkulasi');
});
Route::get('/filesumberdayaair', function () {
    return view('filesumberdayaair');
});
Route::get('/filekalkulasi', function () {
    return view('filekalkulasi');
});
Route::get('/subsumberdayaair', function () {
    return view('subsumberdayaair');
});
Route::get('/proyeksumberdayaair', function () {
    return view('proyeksumberdayaair');
});
Route::get('/changeprofile', function () {
    return view('changeprofile');
});

// admin-section
Route::get('admin', function () {
    return view('admin/login');
});
Route::get('admin/login', function () {
    return view('admin/login');
});
Route::get('admin/home', function () {
    return view('admin/home');
});
Route::get('admin/sumberdayaair', function () {
    return view('admin/sumberdayaair');
});

Route::get('admin/sumberdayaair/newlist', function () {
    return view('admin/sumberdaya/newlist');
});
Route::get('admin/sumberdayaair/publish', function () {
    return view('admin/sumberdaya/publish');
});
Route::get('admin/sumberdayaair/unpublish', function () {
    return view('admin/sumberdaya/unpublish');
});
Route::get('admin/sumberdayaair/edit', function () {
    return view('admin/sumberdaya/edit');
});
Route::get('admin/sumberdayaair/editunpublish', function () {
    return view('admin/sumberdaya/editunpublish');
});
Route::get('admin/ciptakarya', function () {
    return view('admin/ciptakarya');
});
Route::get('admin/ciptakarya/newlist', function () {
    return view('admin/ciptakarya/newlist');
});
Route::get('admin/ciptakarya/edit', function () {
    return view('admin/ciptakarya/edit');
});
Route::get('admin/ciptakarya/publish', function () {
    return view('admin/ciptakarya/publish');
});
Route::get('admin/ciptakarya/unpublish', function () {
    return view('admin/ciptakarya/unpublish');
});
Route::get('admin/ciptakarya/editunpublish', function () {
    return view('admin/ciptakarya/editunpublish');
});
Route::get('admin/userdatabase', function () {
    return view('admin/user');
});
Route::get('admin/userdatabase/sumberdayaair', function () {
    return view('admin/userdatabase/sumberdayaair');
});
Route::get('admin/userdatabase/openfolder', function () {
    return view('admin/userdatabase/open');
});
Route::get('admin/userdatabase/filesumberdayaair', function () {
    return view('admin/userdatabase/filesumberdayaair');
});
Route::get('admin/userdatabase/kalkulasi', function () {
    return view('admin/userdatabase/kalkulasi');
});

Route::get('admin/userdatabase/ciptakarya', function () {
    return view('admin/userdatabase/ciptakarya');
});

Route::get('admin/userdatabase/openciptakarya', function () {
    return view('admin/userdatabase/openciptakarya');
});

Route::get('admin/userdatabase/fileciptakarya', function () {
    return view('admin/userdatabase/fileciptakarya');
});
Route::get('admin/userdatabase/openkalkulasi', function () {
    return view('admin/userdatabase/openkalkulasi');
});
Route::get('admin/membership', function () {
    return view('admin/membership');
});
Route::get('admin/member/payment', function () {
    return view('admin/member/payment');
});
Route::get('admin/member/information', function () {
    return view('admin/member/information');
});
Route::get('admin/createuser', function () {
    return view('admin/createuser');
});
Route::get('admin/user/new', function () {
    return view('admin/user/new');
});
Route::get('admin/user/group', function () {
    return view('admin/user/group');
});
Route::get('admin/user/datafolder', function () {
    return view('admin/user/datafolder');
});
Route::get('admin/user/memberprice', function () {
    return view('admin/user/memberprice');
});
Route::get('admin/user/inputtrial', function () {
    return view('admin/user/inputtrial');
});
Route::get('admin/user/monthprice', function () {
    return view('admin/user/monthprice');
});
Route::get('admin/newadmin', function () {
    return view('admin/newadmin');
});
Route::get('admin/create', function () {
    return view('admin/admin/create');
});
Route::get('admin/admindata', function () {
    return view('admin/admin/admindata');
});