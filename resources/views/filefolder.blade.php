@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto !important;z-index:1000;">
    <nav  class="navbar navbar-expand-md bg-grey" style="padding:0px;width:100%;">
        <div class="container">
            <a class="navbar-brand text-white">File Folder</a>
        </div>
    </nav>
	<div class="container">
		<div class="row align-items-center py-4">
            <div class="col-lg-6 col-7"></div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">Back</a>
              <a href="#" class="btn btn-sm btn-neutral">Home</a>
            </div>
        </div>
		<div class="row" style="padding:20px">
			<div class="col-lg-4 col-sm-6">
				<a href="filesumberdayaair">
					<div class="card card-pricing" data-background="color" data-color="blue">
						<div class="card-body text-center text-white">
							{{-- <h6  class="card-category text-center">File Folder</h6> --}}
							<div class="card-icon">
								<img src="../images/faucet.png" style="color:white" width="100">
							</div>
							<h4 class="card-title text-white">Satuan Pekerjaan Sumber Daya Air</h4>
							<div class="card-footer">
								<a class="btn btn-link" style="color:white !important" href="filesumberdayaair">Show more </a>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-sm-6">
				<a href="ciptakarya">
					<div class="card card-pricing" data-background="color" data-color="blue">
						<div class="card-body text-center">
							{{-- <h6  class="card-category text-white">File Folder</h6> --}}
							<div class="card-icon">
								<img src="../images/architect.png" style="color:white" width="100">
							</div>
							<h4 class="card-title text-white">Satuan Pekerjaan Cipta Karya</h4>
							<div class="card-footer">
								<a class="btn btn-link" style="color:white !important" href="filesumberdayaair">Show more </a>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-sm-6">
				<a href="filekalkulasi">
					<div class="card card-pricing" data-background="color" data-color="blue">
						<div class="card-body text-center text-white">
							{{-- <h6  class="card-category text-white">File Folder</h6> --}}
							<div class="card-icon">
								<img src="../images/calculator (1).png" style="color:white" width="100">
							</div>
							<h4 class="card-title text-white py-5">Kalkulasi Proyek</h4>
							<div class="card-footer">
								<a class="btn-link btn text-white" style="color:white !important" href="filekalkulasi">Show more </a>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>

@endsection

<style>
.card-icon {
    display: block;
    margin: 0 auto;
    position: relative;
    text-align: center;
    padding: 30px 0 10px;
}
.card[data-color=blue] {
	background: #818588;  /* fallback for old browsers */

}
</style>