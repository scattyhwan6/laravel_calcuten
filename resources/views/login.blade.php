@extends('layouts.app')

@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:100vh !important;z-index:1000;background:#3e3e3e;">
     <nav class="navbar navbar-expand-md bg-primary">
        <div class="container">
            <a class="navbar-brand text-white">Login</a>
            <div class="col-md-6 col-sm-12 text-right">
                <ul class="navbar-nav mr-auto pull-right">
                    <li class="nav-item active">
                        <a  class="nav-link" href="#!">Home</a>
                    </li>
                    <li  class="nav-item">
                        <a  class="nav-link" href="#!">Back</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container py-5 mt-6" style="margin-top:5%" >
        <div class="row justify-content-center">
          <div class="col-lg-5 col-md-7">
            <div class="card  border-0 mb-0">
                <div class="card-header bg-transparent ">
                    <div class="text-muted text-center mt-2 mb-4 font-weight-bold">
                        <h5>Sign in using</h5>
                    </div>
                    <div class="form-inline">
                          <div class="btn-wrapper text-center">
                              <a href="#" class="circle google">
                                  <i class="fa fa-google-plus"></i>
                              </a><br>
                              <span class="text-muted">Google</span>
                          </div>
                          <div class="btn-wrapper text-center">
                              <a href="#" class="circle" style="background-color: #455CA8 !important;color: #FFFFFF;">
                                  <i class="fa fa-facebook"></i>
                              </a><br>
                              <span class="text-muted">Facebook</span>
                          </div>
                          <div class="btn-wrapper text-center">
                              <a href="#" class="circle" style="background-color: #00BBFF;color: white;">
                                  <i class="fa fa-twitter"></i>
                              </a><br>
                              <span class="text-muted">Twitter</span>
                          </div>
                    </div>
                </div>
              <div class="card-body px-lg-5 py-lg-5">
                  <div class="text-center text-muted mb-4">
                    <h6>Or </h6>
                  </div>
                  <form role="form">
                    <div class="form-group mb-3">
                        <input class="form-control" placeholder="Email" type="email">
                    </div>
                    <div class="form-group">
                          <input class="form-control" placeholder="Password" type="password">
                    </div>
                    <div class="custom-control custom-control-alternative custom-checkbox">
                        <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                        <label class="custom-control-label" for=" customCheckLogin">
                          <span class="text-muted">Remember me</span>
                        </label>
                    </div>
                    <div class="text-center mt-4">
                      <button type="button" class="btn btn-default btn-md">Sign in</button>
                    </div>
                  </form>
              </div>
            </div>
            <div class="row mt-3">
                <div class="col-6">
                  <a href="#" class="text-white"><span>Forgot password?</span></a>
                </div>
                <div class="col-6 text-right">
                  <a href="#" class="text-white"><span>Create new account</span></a>
                </div>
            </div>
          </div>
        </div>
    </div>
    
</div>


@endsection
<style>

.google {
    background-color: #F74933 !important;
    color: white !important;
}

.circle {
    background-color: #EEEEEE;
    color: #FFFFFF;
    border-radius: 100%;
    display: inline-block;
    margin: 0 17px;
    padding: 23px;
    width: 60px;
    height: 60px;
}
.social {
    float: none;
    margin: 0 auto 30px;
    text-align: center;
}
.circle i {
  font-size: 20px;
}
.btn-wrapper {
  margin: 0 auto;
}
</style>