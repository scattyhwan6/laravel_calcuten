<!DOCTYPE html>
<html>
<head>
	<title>Calculaten</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"> 
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="stylesheet" type="text/css" href="/css/paper-dashboard.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/profilepicture.css">

    <link rel="stylesheet" type="text/css" href="/css/print.min.css">
    <link href="/css/fontawesome.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<body>
		@include('inc.navbar2')
    
    <div class="main-panel section-gray">
       <div class="content">
          @yield('content')
          @include('inc.modal')
        </div>
        
        @include('inc.footer')
        
    </div>
		
		<!-- Scripts -->


<script type="text/javascript" src="{{asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/jquery.printPage.js') }}"></script>
<script type="text/javascript" src="{{asset('js/paper-dashboard.js') }}"></script>
<script type="text/javascript" src="{{asset('js/profilepicture.js') }}"></script>

<style type="text/css" media="print">
 

/* @page {size:landscape}  */ 
@media print {

    @page {size: A4 landscape;max-height:100%; max-width:100%}

body{width:100%;
    height:100%;
   }

</style>

</body>
</html>