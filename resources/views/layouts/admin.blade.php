<!DOCTYPE html>
<html>
<head>
    <title>Calculaten</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"> 
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="stylesheet" type="text/css" href="/css/argon.min.css">
    <link rel="stylesheet" type="text/css" href="/css/styleadmin.css">
    <link href="/css/fontawesome.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<body>
        @include('inc.header')
		@include('inc.nav-admin')
    
        <div class="content">
          @yield('content')
        </div>

<!-- Scripts -->
<script type="text/javascript" src="{{asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.bundle.js') }}"></script>

<script type="text/javascript" src="{{asset('js/argon.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/js.cookie.js') }}"></script>



<style>
body{
    content:"";
    width:100%;
    height:100%;
    display:block; 
    font-size:14px !Important;
}
.navbar{
    border-radius: 0px !important;
}
.table > thead > tr > th {
    border-bottom-width: 0;
    font-size: 15px;
    font-weight: bold;
}
.table td, .table th {
    font-size: 1em;
}
.table-responsive {
    overflow:scroll;
    max-height:500px;
}
.content {
    padding: 0px !important;
}
.btn {
    font-size:12px !important;
}
</style>
</body>
</html>