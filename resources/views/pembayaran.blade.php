@extends('layouts.app')

@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto !important;z-index:1000;display:block;background:#f4f3ef;">
     <nav class="navbar navbar-expand-md bg-grey">
        <div class="container">
            <a class="navbar-brand text-white">Invoice</a>
            <div class="col-md-6 col-sm-12 text-right">
                <ul class="navbar-nav mr-auto pull-right">
                    <li class="nav-item active">
                        <a  class="nav-link" href="#!">Home</a>
                    </li>
                    <li  class="nav-item">
                        <a  class="nav-link" href="#!">Back</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="row py-5">
                <div class="col-xs-12 col-md-4 py-5">
                    <h5 class="invoice-txt">
                        Invoice<strong> #327382</strong>
                    </h5>
                    <div>
                        <h4 class="text-uppercase">Status: <span style="color:red">Unpaid</span></h4>
                        <span>invoice date 23/01/2019</span><br>
                        <span>due date 29/01/2019</span>
                    </div>            
                </div>
                <div class="col-xs-12 col-md-8 pull-right" style="margin:0px">
                    @include('inc.paymentmethod')
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <address>
                                <strong>Invoice To:</strong><br>
                                <p class="mb-0">personal</p>
                                <p>Rambang Basari</p>
                                <p>Jl. Kembang raya no. 11 A Kwitang
                                senen jakarta pusat 10420</p>
                                
                                <abbr title="Phone">P:</abbr> (123) 456-34636
                            </address>
                        </div>
                        <div class="col-md-6 col-sm-6 text-left">
                            <strong>Pay To:</strong>
                            <p class="mb-0 text-success"><strong>PT. Calcuten Inovtekons Nusantara </strong></p>
                            <p>Jl. Kembang raya no. 11 A Kwitang
                                senen jakarta pusat 10420
                            </p>
                            <abbr title="Phone">NPWP:</abbr> 80.2320.203232.3293
                        </div>
                    </div>
            
                    {{-- item Details --}}
                    <div class="row">
                        <div class="table-responsive ">
                            <table class="table table-stripe">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th class="text-center" colspan="2">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="col-md-9">
                                        <em>
                                            12 month membership package<br>
                                            ( 23 may 2019 - 23 may 2020)
                                        </em>
                                    </td>
                                    <td class="col-md-1 text-center">Rp. 1.200.000</td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <p> <strong>Subtotal: </strong></p>
                                        <p><strong>Tax: </strong></p>
                                    </td>
                                    <td class="text-center">
                                        <p><strong>Rp. 1.200.000</strong></p>
                                        <p><strong>10%</strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right"><h5><strong>Total: </strong></h5></td>
                                    <td class="text-center text-danger">
                                        <strong>Rp. 1.200.000</strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        
                    </div>
                    {{-- payment footer --}}
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table ">
                            <thead>
                                <tr>
                                    <th>Transaction order</th>
                                    <th>Gateway</th>
                                    <th colspan="2">Transaction no.</th>
                                    <th colspan="2">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>23 May 2019</td>
                                    <td>30209093209</td>
                                    <td colspan="2">230203</td>
                                    <td>Rp.1.200.000</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2"><b>Balance</b></td>
                                    <td>RP. 1.200.000</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 py-5">
            <div class="mb-5" style="position:sticky;top:30px">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><strong>Cara</strong> pembayaran</h4>
                        <ul>
                            <li>pilih jenis pembayaran yang anda inginkan</li>
                            <li>transfer sesuai dengan instruksi pembayaran yang anda pilih</li>
                            <li>klik paid dan foto lalu upload bukti pembayaran jika anda telah selesai melakukan transaksi pembayaran</li>
                        </ul>
                    </div>
                </div>
                <div class="button-group">
                    {{-- button --}}
                    <hr>
                    <div class="row">
                        <div class="hidden-print col-md-12 text-right">
                            <button class="btn btn-warning btn-icon btn-round" onclick="window.print();">
                                <i class="fa fa-download"></i>
                                download
                            </button>
                            <button class="btn btn-info btn-icon btn-round">
                                <i class="fa fa-share-square"></i>
                                share
                            </button>
                            <button class="btn btn-default btn-icon btn-round" onclick="window.print();">
                                <i class="fa fa-print"></i>
                                print
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<style>
.table th {
    font-size: 1.1rem!important;
    text-transform: capitalize;
    font-weight: 500!important;
    color: #74788d;
    border-top: 0;
    border-bottom: 1px solid #ebedf2;
    padding: 10px 10px 10px 0;
    background-color: transparent;
}
.table tbody tr td {
    background-color: transparent;
    padding: 1rem 0 1rem 0;
    border-top: none;
    font-weight: 400;
    font-size: 1.1rem;
    color: #595d6e;
}
</style>