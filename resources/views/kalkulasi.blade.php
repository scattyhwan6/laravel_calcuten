@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto!important;">
        <nav class="navbar navbar-expand-md bg-grey">
            <div class="container">
                <a class="navbar-brand text-white"> Kalkulasi Proyek</a>
            </div>
        </nav>
        <div class="container">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7"></div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="#" class="btn btn-sm btn-link">Folder</a>
                    <a href="#" class="btn btn-sm btn-link">Back</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    @include('table.kalkulasifolder')
                </div>
                <div class="col-md-4">
                    @include('inc.buttons')
                </div>

            </div>
        </div>


@endsection