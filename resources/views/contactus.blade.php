@extends('layouts.app')

@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;background:cadetblue">
    <nav class="navbar navbar-expand-md" style="background:cadetblue">
        <div class="container">
            <a class="navbar-brand text-white">Contact Us</a>
            <div class="col-md-6 col-sm-12 text-right">
                <ul class="navbar-nav mr-auto pull-right">
                    <li class="nav-item active">
                        <a  class="nav-link" href="#!">Home</a>
                    </li>
                    <li  class="nav-item">
                        <a  class="nav-link" href="#!">Back</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="email-compose-fields">
								<form>
                                    <div class="form-group row">
										<label for="to" class="col-form-label col-md-2">From :</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="from" name="from">
										</div>
									</div>
									<div class="form-group row">
										<label for="to" class="col-form-label col-md-2">To :</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="to" name="to">
										</div>
									</div>
									<div class="form-group row">
										<label for="cc" class="col-form-label col-md-2">Cc :</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="cc" name="cc">
										</div>
									</div>
									<div class="form-group row">
										<label for="subject" class="col-form-label col-md-2">Subject :</label>
										<div class="col-md-10">
											<select class="form-control" id="exampleFormControlSelect1">
                                                <option>Masalah Pembayaran</option>
                                                <option>Masalah User</option>
                                            </select>
										</div>
									</div>
								</form>
                        </div>
                        <div class="col-md-12">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button class="btn btn-link" data-original-title="Bold - Ctrl+B"><i class="icon-bold"></i></button>
                                    <button class="btn btn-link" data-original-title="Italic - Ctrl+I"><i class="icon-italic"></i></button>
                                    <button class="btn btn-link" data-original-title="List"><i class="icon-list"></i></button>
                                    <button class="btn btn-link" data-original-title="Img"><i class="icon-picture"></i></button>
                                    <button class="btn btn-link" data-original-title="URL"><i class="icon-arrow-right"></i></button>
                                </div>
                                <div class="btn-group">
                                    <button class="btn btn-link" data-original-title="Align Right"><i class="icon-align-right"></i></button>
                                    <button class="btn btn-link" data-original-title="Align Center"><i class="icon-align-center"></i></button>
                                    <button class="btn btn-link" data-original-title="Align Left"><i class="icon-align-left"></i></button>
                                </div>
                                <div class="btn-group">
                                    <button class="btn btn-link" data-original-title="Preview"><i class="icon-eye-open"></i></button>
                                    <button class="btn btn-link" data-original-title="Save"><i class="icon-ok"></i></button>
                                    <button class="btn btn-link btn-info" data-original-title="Cancel"><i class="icon-trash"></i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control border-input" placeholder="Write here.." rows="6"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button _ngcontent-c3="" class="btn btn-default btn-round" type="button">send</button>
                        <button _ngcontent-c3="" class="btn btn-danger btn-round" type="button">cancel</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ml-auto mr-auto">
                <div class="row">
                        <div class="info info-horizontal">
                            <div class="icon icon-info">
                                <i aria-hidden="true" class="fa fa-phone"></i>
                            </div>
                            <div  class="description">
                                <h4 class="info-title">Give us a call</h4>
                                <p> 
                                    Michael Jordan<br> 
                                    +40 762 321 762<br> 
                                    Mon - Fri, 8:00-22:00 
                                </p>
                            </div>
                        </div>
                        <div class="info info-horizontal">
                            <div class="icon icon-danger">
                                <i aria-hidden="true" class="fa fa-building"></i>
                            </div>
                            <div class="description">
                                <h4 class="info-title">
                                    Find us at the office</h4>
                                <p> Bld Mihail Kogalniceanu<br> 
                                    7652 Bucharest 
                                </p>
                            </div>
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
@endsection

<style>
.info-horizontal {
    text-align: left;
    margin-top: 0px;
}
.info-horizontal .icon {
   float: left;
    margin-top: 5px;
    margin-right: 45px;
    font-size: 3.5em;
}
.description{
    color:white;
}
.info-horizontal .description {
    overflow: hidden;
}
.icon-danger {
    color: rgb(245, 89, 61);
}
.icon-info {
    color: rgb(81, 188, 218);
}
</style>