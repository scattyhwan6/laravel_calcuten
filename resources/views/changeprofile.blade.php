@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto !important;z-index:1000;background: #797979;">
    <nav  class="navbar navbar-expand-md bg-info" style="padding:0px;width:100%;">
        <div class="container">
            <a class="navbar-brand text-white">Change Profile</a>
            {{-- <ul class="navbar-nav ml-auto">
                <li  class="nav-item">
                    <a href=""class="nav-link"> Home </a>
                </li>
                <li  class="nav-item">
                    <a href=""class="nav-link"> Back </a>
                </li>
            </ul> --}}
        </div>
    </nav>

    <div class="container py-5">
         <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7"></div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">Back</a>
              <a href="#" class="btn btn-sm btn-neutral">Home</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="card card-user">
                    <div class="card-body">
                        <form class="pl-lg-4" novalidate>
                            <div class="row">
                                    <div class="col-md-2 pr-1">
                                        <div class="form-group">
                                            <label> </label>
                                            <select class="form-control" id="exampleFormControlSelect1" required>
                                                <option>Mr.</option>
                                                <option>Mrs.</option>
                                                <option>Ms.</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5 pr-1">
                                        <div class="form-group">
                                            <label class="form-control-label">First Name</label>
                                            <input type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-5 pl-1">
                                        <div class="form-group">
                                            <label class="form-control-label">Last Name</label>
                                            <input type="text" class="form-control" required>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Username</label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Password</label>
                                        <input type="password" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Confirm</label>
                                        <input type="password" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Your Email</label>
                                        <input type="email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Handphone</label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Company / Institution</label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Occupation</label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Email Address</label>
                                        <input type="email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-3 px-1">
                                    <div class="form-group">
                                        <label class="form-control-label">City</label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-3 pl-1">
                                    <div class="form-group">
                                        <label class="form-control-label">State</label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Country</label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Zip</label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label class="form-control-label">Office Phone</label>
                                        <input type="text" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="card card-user">
                    <div class="image">
                        <img src="../images/profile-bg.jpg" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            {{-- <app-image-upload>
                                 <div _ngcontent-c14="" class="fileinput text-center">
                                <div _ngcontent-c14="" class="thumbnail img-no-padding img-circle">
                                    <img _ngcontent-c14="" alt="..." src="../images/placeholder.jpg">
                                </div>
                                <div _ngcontent-c14=""><!---->
                                    <button _ngcontent-c14="" class="btn btn-outline-default btn-file btn-round ng-star-inserted">Select image</button><!----><!---->
                                </div>
                            </div>
                            </app-image-upload> --}}
                           
                            <div class="profile">
                                <div class="photo">
                                    <input type="file" accept="image/*">
                                    <div class="photo__helper">
                                        <div class="photo__frame photo__frame--circle">
                                            <canvas class="photo__canvas"></canvas>
                                            <div class="message is-empty">
                                                <p class="message--desktop">Drop your photo here or browse your computer.</p>
                                                <p class="message--mobile">Tap here to select your picture.</p>
                                            </div>
                                            <div class="message is-wrong-file-type">
                                                <p>Only images allowed.</p>
                                                <p class="message--desktop">Drop your photo here or browse your computer.</p>
                                                <p class="message--mobile">Tap here to select your picture.</p>
                                            </div>
                                            <div class="message is-wrong-image-size">
                                                <p>Your photo must be larger than 350px.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="photo__options hide">
                                        <div class="photo__zoom">
                                            <input type="range" class="zoom-handler">
                                        </div><a href="javascript:;" class="remove"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            </div> 
                        </div>
                            
                    </div>
                    <div class="card-footer text-center">
                        <button type="button" id="uploadBtn" class="btn btn-default btn-round">Save</button>
                        <button type="button" class="btn btn-danger btn-round">Cancel</button>
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection
