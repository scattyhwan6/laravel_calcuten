@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto !important;z-index:1000;background:lightgray">

<nav class="navbar navbar-expand-md bg-grey" style="padding-top:0px;">
    <div class="container">
        <a class="navbar-brand text-white">File Folder / Sumber Daya Air / Cth Turap Banjir Kanal Timur</a>
            
    </div>
</nav>
<div class="container">
    <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7"></div>
        <div class="col-lg-6 col-5 text-right">
            <div style="width: 60%;margin: 0;float: left;">
                <form style="margin-top:20px">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control mr-sm-2 no-border" placeholder="Search...">
                        <button class="btn btn-primary btn-just-icon btn-round" type="submit">
                            <i aria-hidden="true" class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
            </div>
            <div class="mt-6 py-5" style="float:left">
                <a href="#" class="btn btn-link">Back</a>
            </div> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div id="table-scroll">
                    <section class="citakarya_list">
                        <ul class="list-group ">
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="proyeksumberdayaair"> 
                                    <strong>Sumber daya air</strong>  
                                    <small class="pull-right text-white">15 April</small>
                                    <span>1 m2 Turap Banjir Kanal Timur </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>

                        </ul>
                    </section>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-4">
            <div class="card card-circle-chart" data-background-color="blue">
                <div class="card-header text-center">
                    <h3 class="card-title">User Note:</h3>
                </div>
                <div class="card-content">
                    <ol class="text-left">
                        <li><p class="description">Klik analisa pekerjaan yang akan anda kerjakan.</p></li>
                        <li><p class="description">Bagi member trial hanya akan dapat menggunakan 10 analisa satuan teratas, selama masa trial</p></li>
                    </ol>
                </div>
            </div>
        </div> --}}
    </div>
</div>

<section>
         <!-- The Modal -->
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Share to</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <button class="btn btn-icon btn-fill btn-twitter">
                            <i class="fa fa-download"></i>
                        </button>
                        <button class="btn btn-icon btn-fill btn-twitter">
                            <i class="fa fa-folder"></i>
                        </button>
                        <button class="btn btn-icon btn-fill btn-twitter">
                            <i class="fa fa-whatsapp-square"></i>
                        </button>
                        <button class="btn btn-icon btn-fill btn-twitter">
                            <i class="fa fa-mail"></i>
                        </button>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
         <!-- The Modal -->
        <div class="modal" id="saveModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Save to</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <button class="btn btn-icon btn-fill btn-twitter">
                            <i class="fa fa-download"></i>
                        </button>
                        <button class="btn btn-icon btn-fill btn-twitter">
                            <i class="fa fa-folder"></i>
                        </button>
                        <button class="btn btn-icon btn-fill btn-twitter">
                            <i class="fa fa-whatsapp-square"></i>
                        </button>
                        <button class="btn btn-icon btn-fill btn-twitter">
                            <i class="fa fa-mail"></i>
                        </button>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection

<script>
function myFunction() {
  window.print();
}

</script>

<style>
.modal-backdrop {
    background-color: #68b3c873 !important;
}
.table-responsive {
    overflow:scroll;
    max-height:400px;
}
#table-scroll{
    overflow:scroll !important;
    max-height:500px !important;
}
.list-group-item {
    background-color: #46989a !important
}
.list-group-item span {
    color:white;
}
</style>

