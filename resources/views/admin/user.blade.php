@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
      <div class="container">
        <div class="header-body">
          <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h1 class="text-white">User Database</h1>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <div class="mb-5">
                        <a href="#" class="btn btn-sm btn-neutral">Home</a>
                        <a href="#" class="btn btn-sm btn-neutral">Back</a><br>
                    </div>
                    <div class="pull-right search"><input class="form-control" type="text" placeholder="Search"></div>
                </div>
          </div>
        </div>
      </div>
    </div>
<div class="container mt--6">
    <div class="card ">
        <div class="table-responsive" >
          <table class="table align-items-center table-flush table-striped">
            <thead class="thead-light">
              <tr>
                <th scope="col" class="sort" data-sort="usernumber">User Number</th>
                <th scope="col" class="sort" data-sort="nama">Nama</th>
                <th scope="col">Sumber Daya Air</th>
                <th scope="col">Cipta Karya</th>
                 <th scope="col">Kalkulasi Proyek</th>
              </tr>
            </thead>
            <tbody class="list">
                <tr>
                    <td class="budget"> 000111</td>
                    <td>Ir. Mulyadi</td>
                    <td>
                       
                        <a href="{{URL::to('admin/userdatabase/sumberdayaair')}}" class="font-weight-bold">Open Folder</a>
                    </td>
                    <td><a href="{{URL::to('admin/userdatabase/ciptakarya')}}" class="font-weight-bold">Open Folder</a></td>
                    <td><a href="{{URL::to('admin/userdatabase/kalkulasi')}}" class="font-weight-bold">Open Folder</a></td>
                </tr>
                <tr>
                    <td class="budget"> 000112</td>
                    <td>Syamsul bahri</td>
                    <td><a href="{{URL::to('admin/userdatabase/sumberdayaair')}}" class="font-weight-bold">Open Folder</a> </td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                </tr>
                <tr>
                    <td class="budget"> 000113</td>
                    <td>Rosi Rosalinda</td>
                    <td><a href="{{URL::to('admin/userdatabase/sumberdayaair')}}" class="font-weight-bold">Open Folder</a> </td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                </tr>
                <tr>
                    <td class="budget"> 000114</td>
                    <td>Anggara Baskoro</td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a> </td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                </tr>
                <tr>
                    <td class="budget"> 000117</td>
                    <td>Louis Andika Pratama</td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a> </td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                </tr>
                <tr>
                    <td class="budget"> 000118</td>
                    <td>Ir. Tri Ferdianto</td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a> </td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                    <td><a href="#!" class="font-weight-bold">Open Folder</a></td>
                </tr>
                
            </tbody>
          </table>
        </div>
      </div>
</div>


@endsection