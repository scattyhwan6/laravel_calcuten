@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
        <div class="header-body pb-6">
             <div class="header pb-6">
                  <div class="container">
                    <div class="header-body">
                      <div class="row align-items-center py-4">
                          <div class="col-lg-6 col-7">
                            <h1 class=" text-white d-inline-block mb-0">User Membership</h1><br>
                            <span class="text-white btn btn-sm btn-default mr-4">Payment</span>   
                          </div>
                          <div class="col-lg-6 col-5 text-right">
                            <a href="#" class="btn btn-sm btn-neutral">Home</a>
                            <a href="#" class="btn btn-sm btn-neutral">Back</a>
                          </div>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="row align-items-center py-4">
                <div class="col-lg-4">
                      <div class="form-group">
                          <label class="text-white" for="membership">Select Membership</label>
                          <select id="membership" type="search" class="select-table-filter form-control caret" data-table="order-table" placeholder="Select Membership">
                            <option>Enterprise</option>  
                            <option>Starter</option>  
                            <option>Business</option>  
                          <select>
                      </div>
                      <div class="form-group col-md-6" style="margin:0px;float:left">
                          <label class="text-white" for="">Invoice</label>
                          <select type="search" class="select-table-filter form-control" data-table="order-table">
                              <option value="">Invoice</option>
                              <option>Paid</option>  
                              <option>Unpaid</option>
                          <select>
                      </div>
                      <div class="form-group col-md-6" style="margin:0px;float:left">
                          <label class="text-white" for="">Select Time</label>
                          <select type="search" class="select-table-filter form-control" data-table="order-table">
                              <option value="">Select Time</option>
                              <option>7 hari terakhir</option>  
                              <option>30 hari terakhir</option>
                          <select>
                      </div>
                </div>
                <div class="col-lg-4 col-5 text-right">
                    <div class="form-group">
                        <div class="input-group input-group-merge">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-search"></i></span>
                            </div>
                            <input type="search" class="light-table-filter form-control" data-table="order-table" placeholder="Invoice Number.." />
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-5 text-right">
                    <div class="form-group">
                        <label class="form-control-label  text-white" for="example3cols3Input">Total Transaksi (Rupiah)</label>
                        <input type="text" class="form-control" id="example3cols3Input" placeholder="Ex : 2.700.000">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt--6">
    <div class="row">
        <!-- Card header -->
        <div class="col-md-12">
            <div class="row mb-3 text-white">
                  <div class="col-6">
                      tgl / bln / tahun
                  </div>
                  <div class="col-6 text-right">30 Hari Terakhir</div>
            </div>
             @include ('admin.table.tabmember')
        </div>
       
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="invoicemodal" tabindex="-1" role="dialog" aria-labelledby="invoicemodal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        {{-- modal-header-invoice --}}
        <div class="modal-header header bg-gradient-default text-white">
            <h6 class="modal-title text-white" id="invoicemodal">
            <span class="">Invoice</span> #23827382</h6>
        
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="text-white">&times;</span>
            </button>
        </div>
        <div class="modal-header" style="border-radius:0px !important">
            <div class="col-lg-6 col-7">
                <h3 class="font-weight-bold">Status: <span class="text-danger">Unpaid</span></h3>
                <p class="mb-0">Invoice date : 23/01/2019</p>
                <p class="mb-0">Due date : 30/01/2019</p>
            </div>
            <div class="col-lg-6 col-5 text-right">
                <a href="#!" class=" share" style="font-size:16px;">
                    <button class="btn btn-default btn-icon-only rounded-circle">
                      <i class="fa fa-share-alt-square"></i> 
                    </button>
                </a>
                <button class="btn btn-default  btn-icon-only rounded-circle">
                    <i class="fa fa-download"></i>
                </button>
            </div>
        </div>
        {{-- invoice content --}}
        <div class="modal-body">
              <div class="row align-items-center">
                  <div class="col-lg-6 col-7">
                      <h4 class="font-weight-bold">Invoice to :</h4>
                      <p class="mb-0">Personal</p>
                      <p class="mb-0">Rambang basari</p>
                      <p class="mb-0">Jl. Kembangan raya no. 11A</p>
                      <p class="mb-0">Jl. Kembangan raya no. 11A</p>
                  </div>
                  <div class="col-lg-6 col-5 text-left">
                      <h4 class="font-weight-bold">Pay to :</h4>
                      <p class="mb-0 title">PT. Adyatama digital nusantara</p>
                      <p class="mb-0 address">Jl. Kembangan raya no. 11A, Kwitang</p>
                      <p class="mb-0">Senen Jakarta pusat 10420</p>
                      <p class="small">NPWP 80.280.657.9-542.000</p>
                  </div>
              </div>
              <div class="card">
                 
                  <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                      <thead class="thead-light">
                          <tr>
                              <th>Description</th>
                              <th>Amount</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td>Enterprise Membership package - Rambang  <br>- 1 year (23/01/2019-23/01/2020)</td>
                            <td>Rp. 599.000,00</td>
                          </tr>
                          <tr>
                            <td class="text-right"><b>sub total</b></td>
                            <td>Rp. 599.000,00</td>
                          </tr>
                          <tr>
                            <td class="text-right"><b>credit</b></td>
                            <td>Rp. 00,00</td>
                          </tr>
                          <tr>
                            <td class="text-right"><b>Total</b></td>
                            <td>Rp. 699.000,00</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
              <div class="card">
                  <!-- Light table -->
                  <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                      <thead class="thead-light">
                          <tr>
                              <th>Transaction date</th>
                              <th>Gateway</th>
                              <th>Transaction ID</th>
                              <th>Amount</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td colspan="4">No related transaction found</td>
                          </tr>
                          <tr>
                              <td colspan="3"  class="text-right"><b>Balance</b></td>
                              <td colspan="3">Rp 699.000,00</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
        </div>
    </div>
  </div>
</div>

@endsection
<style>
.share {
  margin-right: 10px;
  font-size: 16px;
}
.share i {
  font-size: 20px !important;
}
p.title {
  font-size: 14px !important;
  font-weight: 400;
}
p.address {
  font-size: 12px !important;
  font-weight: 400;
}
</style>