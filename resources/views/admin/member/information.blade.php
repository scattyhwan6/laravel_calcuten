@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h1 class="display-3 text-white d-inline-block mb-0">User Membership - Information</h1><br>          
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Home</a>
            <a href="#" class="btn btn-sm btn-neutral">Back</a>
            <br>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container mt--6">
    <div class="card">
        <!-- Light table -->
        <div class="table-responsive table-striped">
          <table class="table align-items-center ">
            <thead class="thead-light">
              <tr>
                <th>User<br>Number</th>
                <th>Nama</th>
                <th>Jenis Member</th>
                <th>Sisa Durasi<br> (days)</th>
                <th>Aktif Sejak</th>
                <th>Aktif Sampai</th>
                <th>User Data</th>
              </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="table-number">000111</td>
                    <td>
                        <span class="text-muted">Ir. Mulyadi Setiadi</span>
                    </td>
                    <td>
                        Enterprise
                    </td>
                    <td>254</td>
                    <td>D/M/Y</td>
                    <td>D/M/Y</td>
                    <td>
                        <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-folder"></i>
                        </a>
                    </td>
                </tr>
                <tr class="table-warning">
                    <td class="table-number">000111</td>
                    <td>
                        <span class="text-muted">Ir. Mulyadi Setiadi</span>
                    </td>
                    <td>
                        Enterprise
                    </td>
                    <td>254</td>
                    <td>D/M/Y</td>
                    <td>D/M/Y</td>
                    <td>
                        <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-folder"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="table-number">000111</td>
                    <td>
                        <span class="text-muted">Ir. Mulyadi Setiadi</span>
                    </td>
                    <td>
                        Enterprise
                    </td>
                    <td>254</td>
                    <td>D/M/Y</td>
                    <td>D/M/Y</td>
                    <td>
                        <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-folder"></i>
                        </a>
                    </td>
                </tr>
                <tr class="table-warning">
                    <td class="table-number">000111</td>
                    <td>
                        <span class="text-muted">Ir. Mulyadi Setiadi</span>
                    </td>
                    <td>
                        Enterprise
                    </td>
                    <td>254</td>
                    <td>D/M/Y</td>
                    <td>D/M/Y</td>
                    <td>
                        <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-folder"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="table-number">000111</td>
                    <td>
                        <span class="text-muted">Ir. Mulyadi Setiadi</span>
                    </td>
                    <td>
                        Enterprise
                    </td>
                    <td>254</td>
                    <td>D/M/Y</td>
                    <td>D/M/Y</td>
                    <td>
                        <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-folder"></i>
                        </a>
                    </td>
                </tr>
                <tr class="table-warning">
                    <td class="table-number">000111</td>
                    <td>
                        <span class="text-muted">Ir. Mulyadi Setiadi</span>
                    </td>
                    <td>
                        Enterprise
                    </td>
                    <td>254</td>
                    <td>D/M/Y</td>
                    <td>D/M/Y</td>
                    <td>
                        <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-folder"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="table-number">000111</td>
                    <td>
                        <span class="text-muted">Ir. Mulyadi Setiadi</span>
                    </td>
                    <td>
                        Enterprise
                    </td>
                    <td>254</td>
                    <td>D/M/Y</td>
                    <td>D/M/Y</td>
                    <td>
                        <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-folder"></i>
                        </a>
                    </td>
                </tr>
                <tr class="table-warning">
                    <td class="table-number">000111</td>
                    <td>
                        <span class="text-muted">Ir. Mulyadi Setiadi</span>
                    </td>
                    <td>
                        Enterprise
                    </td>
                    <td>254</td>
                    <td>D/M/Y</td>
                    <td>D/M/Y</td>
                    <td>
                        <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-folder"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="table-number">000111</td>
                    <td>
                        <span class="text-muted">Ir. Mulyadi Setiadi</span>
                    </td>
                    <td>
                        Enterprise
                    </td>
                    <td>254</td>
                    <td>D/M/Y</td>
                    <td>D/M/Y</td>
                    <td>
                        <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-folder"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
          </table>
        </div>
    </div>
</div>


{{-- Modal --}}
<div class="row">
    <div class="col-md-4">
        <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
            <div class="modal-dialog modal-default modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header header bg-gradient-success">
                        <h3 class="modal-title display-3" id="modal-title-default">Member Data </h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-0">
                            <p class="col-md-3 font-weight-bold">Name</p>
                            <div class="col-md-9">
                                <p class="">: Ir.Mulyadi Sutedja</p>
                            </div>
                        </div>
                         <div class="row mb-0">
                            <p class="col-md-3 font-weight-bold">Perusahaan</p>
                            <div class="col-md-9">
                                <p class="">: Hutama Karya</p>
                            </div>
                        </div>
                        <div class="row mb-0">
                            <p class="col-md-3 font-weight-bold">Alamat</p>
                            <div class="col-md-9">
                                <p class="">: Jl. MT Haryono Kav 1 Cawang Jakarta Timur</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-3 font-weight-bold">PO Box</p>
                            <div class="col-md-9">
                                <p class="">: 14560</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-3 font-weight-bold">Email</p>
                            <div class="col-md-9">
                                <p class="">: mulyaditedja@gmail.com</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-3 font-weight-bold">Phone</p>
                            <div class="col-md-9">
                                <p class="">: 0812892900</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-3 font-weight-bold">Fax</p>
                            <div class="col-md-9">
                                <p class="">: 021 227382</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-3 font-weight-bold">Mobile</p>
                            <div class="col-md-9">
                                <p class="">: 0812892900</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-3 font-weight-bold">Membership</p>
                            <div class="col-md-9">
                                <p class="">: Platinum</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-3 font-weight-bold">Aktifasi</p>
                            <div class="col-md-9">
                                <p class="">: 01 januari 2019</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-3 font-weight-bold">Akhir Aktifasi</p>
                            <div class="col-md-9">
                                <p class="">: 01 januari 2020</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        
                        <button type="button" class="btn btn-link">
                            <i class="fa fa-share-alt-square text-white" style="font-size:20px"></i>
                        </button>
                        <button type="button" class="btn btn-link btn-default">
                            <i class="fa fa-download text-white" style="font-size:20px"></i>
                        </button>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection