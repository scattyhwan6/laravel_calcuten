@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h2 class="display-4 text-white d-inline-block mb-0">Group Name</h2><br>   
            <h2 class="display-4 text-white d-inline-block mb-0">Invoices Number</h2><br>          
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Home</a>
            <a href="#" class="btn btn-sm btn-neutral">Back</a>
            <br>
        </div>
        </div>
    </div>
    </div>
</div>

<div class="container mt--6">
    <div class="card">
        <!-- Light table -->
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                <tr>
                    <th>No User</th>
                    <th>Nama User</th>
                    <th>Email User</th>
                    <th>Pass</th>
                    <th>Membership</th>
                    <th>Active</th>
                    <th>Billing</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>900018</td>
                        <td>Wika 01</td>
                        <td>None</td>
                        <td>Wika111</td>
                        <td>Enterprise</td>
                        <td>04/02/2019</td>
                        <td>
                            Rp 0,00
                        </td>
                        <td class="table-actions">
                            <a href="#!" class="table-action" data-toggle="modal" data-target="">
                                <i class="fa fa-user"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#modal-notification">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

{{-- modal --}}
<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Type your modal title</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
                
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
    <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-danger">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-notification">Your attention is required</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="py-3 text-center">
                    <i class="fa fa-exclamation-circle fa-3x"></i>
                    <h4 class="heading mt-4">Are you sure?</h4>
                    <p>You won't be able to revert this!</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white">Yes, deleted it</button>
                <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Cancel</button>
            </div>
            
        </div>
    </div>
@endsection