@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h1 class="display-3 text-white d-inline-block mb-0">Create User - Group User Data</h1><br>          
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Home</a>
            <a href="#" class="btn btn-sm btn-neutral">Back</a>
            <br>
        </div>
        </div>
    </div>
    </div>
</div>

<div class="container mt--6">
    <div class="card">
        <!-- Light table -->
        <div class="table-responsive">
            <table class="table align-items-center table-striped">
                <thead class="thead-light">
                <tr>
                    <th>Tgl Pembuatan</th>
                    <th>Pembuat</th>
                    <th>Nama Group</th>
                    <th>Invoices Number</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <span class="text-muted">10/09/2018</span>
                        </td>
                        <td>
                            Rambang Basari
                        </td>
                        <td>
                            Cth : Penyidik KPK
                        </td>
                        <td class="table-actions">
                            CTH 973819202000
                        </td>
                        <td class="table-actions">
                            <a href="{{URL::to('admin/user/datafolder')}}" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete product">
                                <i class="fa fa-folder"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="text-muted">10/09/2018</span>
                        </td>
                        <td>
                            Rambang Basari
                        </td>
                        <td>
                            Cth : Penyidik KPK
                        </td>
                        <td class="table-actions">
                            CTH 973819202000
                        </td>
                        <td class="table-actions">
                            <a href="{{URL::to('admin/user/datafolder')}}" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete product">
                                <i class="fa fa-folder"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="text-muted">10/09/2018</span>
                        </td>
                        <td>
                            Rambang Basari
                        </td>
                        <td>
                            Cth : Penyidik KPK
                        </td>
                        <td class="table-actions">
                            CTH 973819202000
                        </td>
                        <td class="table-actions">
                            <a href="{{URL::to('admin/user/datafolder')}}" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete product">
                                <i class="fa fa-folder"></i>
                            </a>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <span class="text-muted">10/09/2018</span>
                        </td>
                        <td>
                            Rambang Basari
                        </td>
                        <td>
                            Cth : Penyidik KPK
                        </td>
                        <td class="table-actions">
                            CTH 973819202000
                        </td>
                        <td class="table-actions">
                            <a href="{{URL::to('admin/user/datafolder')}}" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete product">
                                <i class="fa fa-folder"></i>
                            </a>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <span class="text-muted">10/09/2018</span>
                        </td>
                        <td>
                            Rambang Basari
                        </td>
                        <td>
                            Cth : Penyidik KPK
                        </td>
                        <td class="table-actions">
                            CTH 973819202000
                        </td>
                        <td class="table-actions">
                            <a href="{{URL::to('admin/user/datafolder')}}" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete product">
                                <i class="fa fa-folder"></i>
                            </a>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <span class="text-muted">10/09/2018</span>
                        </td>
                        <td>
                            Rambang Basari
                        </td>
                        <td>
                            Cth : Penyidik KPK
                        </td>
                        <td class="table-actions">
                            CTH 973819202000
                        </td>
                        <td class="table-actions">
                            <a href="{{URL::to('admin/user/datafolder')}}" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete product">
                                <i class="fa fa-folder"></i>
                            </a>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <span class="text-muted">10/09/2018</span>
                        </td>
                        <td>
                            Rambang Basari
                        </td>
                        <td>
                            Cth : Penyidik KPK
                        </td>
                        <td class="table-actions">
                            CTH 973819202000
                        </td>
                        <td class="table-actions">
                            <a href="{{URL::to('admin/user/datafolder')}}" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete product">
                                <i class="fa fa-folder"></i>
                            </a>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <span class="text-muted">10/09/2018</span>
                        </td>
                        <td>
                            Rambang Basari
                        </td>
                        <td>
                            Cth : Penyidik KPK
                        </td>
                        <td class="table-actions">
                            CTH 973819202000
                        </td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete product">
                                <i class="fa fa-folder"></i>
                            </a>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <span class="text-muted">10/09/2018</span>
                        </td>
                        <td>
                            Rambang Basari
                        </td>
                        <td>
                            Cth : Penyidik KPK
                        </td>
                        <td class="table-actions">
                            CTH 973819202000
                        </td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete product">
                                <i class="fa fa-folder"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
<style>
.table-striped tbody tr:nth-of-type(odd) {
    background-color: #abcb57 !important;
    color: whitesmoke !important;
}
</style>