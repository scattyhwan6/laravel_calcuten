@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
      <div class="container">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Input Trial</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Create and change member price</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Input trial</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div>
          </div>
        </div>
      </div>
    </div>
<div class="container mt--6">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <!-- Card body -->
                <div class="card-body">
                    <form>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Durasi trial</label>
                            <div class="col-md-8">
                                <button type="button" class="btn btn-outline-default">7D</button>
                                <button type="button" class="btn btn-outline-default">14D</button>
                                <button type="button" class="btn btn-outline-default">21D</button>
                                <button type="button" class="btn btn-outline-default">30D</button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Akses sub page cipta karya</label>
                            <div class="col-md-8">
                                <button type="button" class="btn btn-outline-primary">Choose max 10 page</button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Akses sub page sumber daya air</label>
                            <div class="col-md-8">
                                <button type="button" class="btn btn-outline-default">Choose max 10 page</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-primary" type="button">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection