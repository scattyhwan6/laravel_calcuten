@extends('layouts.admin')


@section('content')
<div class="header  pb-6">
    <div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h2 class="display-4 d-inline-block mb-0">Create and change member price </h2><br>         
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Home</a>
            <a href="#" class="btn btn-sm btn-neutral">Back</a>
            <br>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="card">
            <!-- Card body -->
            <div class="card-body">
                <!-- List group -->
                <ul class="list-group list-group-flush list my--3">
                    <li class="list-group-item px-0">
                        <div class="row align-items-center">
                            <div class="col ml--2">
                                <h4 class="mb-0">
                                    <a href="{{URL::to('admin/user/inputtrial')}}">Input Trial</a>
                                </h4>
                            </div>
                            <div class="col-auto">
                               
                                <a href="{{URL::to('admin/user/inputtrial')}}">Edit</a>
                               
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item px-0">
                        <div class="row align-items-center">
                            <div class="col ml--2">
                                <h4 class="mb-0">
                                <a href="{{URL::to('admin/user/monthprice')}}">Input 1 month prices</a>
                                </h4>
                            </div>
                            <div class="col-auto">
                                
                                <a href="{{URL::to('admin/user/monthprice')}}">Edit</a>    
                                
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item px-0">
                        <div class="row align-items-center">
                            <div class="col ml--2">
                                <h4 class="mb-0">
                                <a href="{{URL::to('admin/user/monthprice')}}">Input 12 month prices</a>
                                </h4>
                            </div>
                            <div class="col-auto">
                                <a href="{{URL::to('admin/user/monthprice')}}">Edit</a> 
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item px-0">
                        <div class="row align-items-center">
                            <div class="col ml--2">
                                <h4 class="mb-0">
                                <a href="#!">Input 12 month prices</a>
                                </h4>
                            </div>
                            <div class="col-auto">
                                 <a href="{{URL::to('admin/user/monthprice')}}">Edit</a> 
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>




@endsection