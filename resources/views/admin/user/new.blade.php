@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h1 class="display-3 text-white d-inline-block mb-0">Create New User</h1><br>          
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Home</a>
            <a href="#" class="btn btn-sm btn-neutral">Back</a>
            <br>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <div class="card mt--6">
                <!-- Card body -->
                <div class="card-body">
                    <form>
                        <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label form-control-label">New Group</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" id="newgroup">
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="example-search-input" class="col-md-2 col-form-label form-control-label">Nama User</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="search"  id="example-search-input">
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="example-email-input" class="col-md-2 col-form-label form-control-label">Email User</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="email"  id="example-email-input">
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label form-control-label">Pass User</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="password" id="newgroup">
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label form-control-label">Jenis member</label>
                                <div class="col-md-10 form-inline">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" id="starter" type="checkbox">
                                        <label class="custom-control-label" for="starter" style="padding-left: 0px;">Starter</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" id="Business" type="checkbox">
                                        <label class="custom-control-label" for="Business" style="padding-left: 0px;">Business</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" id="Enterprise" type="checkbox">
                                        <label class="custom-control-label" for="Enterprise" style="padding-left: 0px;">Enterprise</label>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label form-control-label">Waktu Aktivasi</label>
                                <div class="col-md-10 form-inline">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" id="Sekarang" type="checkbox">
                                        <label class="custom-control-label" for="Sekarang">Aktivasi Sekarang</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" id="Nanti" type="checkbox">
                                        <label class="custom-control-label" for="Nanti">Aktivasi Nanti</label>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Default select</option>
                                        </select>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="Invoices" class="col-md-2 col-form-label form-control-label">Invoices Num.</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text"  id="Invoices">
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="Billing" class="col-md-2 col-form-label form-control-label">Billing</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text"  id="Billing">
                                </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer text-right">
                <button class="btn btn-default" type="button">save and create new</button>
                <button class="btn btn-icon btn-primary" type="button">
                    <span class="btn-inner--icon"><i class="fa fa-cancel"></i></span>
                    <span class="btn-inner--text">Cancel</span>
                </button>
                <button class="btn btn-icon btn-primary" type="button">
                    <span class="btn-inner--text">Back</span>
                </button>
                <button class="btn btn-icon btn-primary" type="button">
                    <span class="btn-inner--text">Home</span>
                </button>
            </div>
            </div>
           
        </div>
    </div>
</div>
@endsection