@extends('layouts.admin')


@section('content')

<div class="header d-flex align-items-center" style="min-height: 500px; background-image: url(../../images/writing.jpg); background-size: cover; background-position: center top;">   
    <div class="container">
        <span class="mask bg-gradient-default opacity-8"></span>
        <!-- Header container -->
        <div class="container-fluid d-flex align-items-center">
            <div class="row">
                <div class="col-lg-7 col-md-10">
                    <h1 class="display-2 text-white mb-5">User Membership</h1>
                </div>
            </div>
        </div>
    </div>   
</div>
<div class="container mt--6">
    <div class="col-xl-8 order-xl-1">
          <div class="row">
            <div class="col-lg-6">
                <div class="card bg-gradient-success border-0">
                    <!-- Card body -->
                    <div class="card-body text-white">
                        <div class="row">
                            <div class="col">
                                <span class="h2 font-weight-bold mb-0 text-white">User Payment</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                    <img src="../images/pay.png" width="55">
                                </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                            <a href="{{URL::to('admin/member/payment')}}" class="btn btn-md btn-secondary">Click Here</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card bg-gradient-danger border-0">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                                <div class="col">
                                    <span class="h2 font-weight-bold mb-0 text-white">User Information</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                            <img src="../images/support.png" width="55">
                                    </div>
                                </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                                <a href="{{URL::to('admin/member/information')}}" class="btn btn-md btn-secondary">Click Here</a>
                        </p>
                    </div>
                </div>
            </div>
          </div>
    </div>
</div>



@endsection
<style>
p {
    font-size:1.5em!important;
}
.btn{
    font-size:14px!important;
}
.custom-control-label::before, 
.custom-control-label::after {
top: 1rem;
width: 2.25rem !important;
height: 2.25rem !important;
}
</style>