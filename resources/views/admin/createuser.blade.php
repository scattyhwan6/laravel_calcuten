@extends('layouts.admin')


@section('content')

<div class="header d-flex align-items-center" style="min-height: 500px; background-image: url(../../images/writing.jpg); background-size: cover; background-position: center top;">   
    <div class="container">
        <span class="mask bg-gradient-default opacity-8"></span>
        <!-- Header container -->
        <div class="container-fluid d-flex align-items-center">
            <div class="row">
                <div class="col-lg-7 col-md-10">
                    <h1 class="display-3 text-white mb-5">Create User</h1>
                    <h1 class="display-4 text-white">Hello Calculaten Team,</h1>
                    <p class="text-white mt-0 mb-5">This is your  page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>  
                </div>
            </div>
        </div>
    </div>   
</div>
<div class="container mt--6">
    <div class="col-xl-8 order-xl-1">
          <div class="row">
                <div class="col-lg-6">
                    <div class="card bg-gradient-info border-0">
                        <!-- Card body -->
                        <div class="card-body text-white">
                            <div class="row">
                                <div class="col">
                                    <span class="h2 font-weight-bold mb-0 text-white">Create New User</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                        <img src="../images/user1.svg" width="50">
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                <a href="{{URL::to('admin/user/new')}}" class="btn btn-md btn-default">Click Here</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card bg-gradient-danger border-0">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                    <div class="col">
                                        <span class="h2 font-weight-bold mb-0 text-white">Group User Data</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                <img src="../images/user.svg" width="50">
                                        </div>
                                    </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                    <a href="{{URL::to('admin/user/group')}}" class="btn btn-md btn-default">Click Here</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card bg-gradient-primary border-0">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                    <div class="col">
                                        <span class="h2 font-weight-bold mb-0 text-white">Create Member Price</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                <img src="../images/copywriter.png" width="50">
                                        </div>
                                    </div>
                            </div>
                            <p class="mt-3 mb-0 text-sm">
                                    <a href="{{URL::to('admin/user/memberprice')}}" class="btn btn-md btn-default">Click Here</a>
                            </p>
                        </div>
                    </div>
                </div>
          </div>
    </div>
</div>



@endsection
<style>
p {
    font-size:1.5em!important;
}
.btn{
    font-size:14px!important;
}
.custom-control-label::before, 
.custom-control-label::after {
top: 1rem;
width: 2.25rem !important;
height: 2.25rem !important;
}
</style>