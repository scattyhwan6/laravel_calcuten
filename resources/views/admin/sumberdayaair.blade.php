@extends('layouts.admin')


@section('content')

<div class="header d-flex align-items-center" style="min-height: 500px; background-image: url(../../images/writing.jpg); background-size: cover; background-position: center top;">   
    <div class="container">
        <span class="mask bg-gradient-default opacity-8"></span>
        <!-- Header container -->
        <div class="container-fluid d-flex align-items-center">
            <div class="row">
                <div class="col-lg-7 col-md-10">
                    <h1 class="display-3 text-white mb-5">Sumber Daya Air</h1>
                    <h1 class="display-5 text-white">Hello Calculaten Team,</h1>
                    <p class="text-white mt-0 mb-5">This is your  page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>  
                </div>
            </div>
        </div>
    </div>   
</div>
<div class="container mt--6">
    <div class="col-xl-8 order-xl-1">
          <div class="row">
            <div class="col-lg-6">
                <div class="card bg-gradient-info border-0">
                    <!-- Card body -->
                    <div class="card-body text-white">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted text-white">Sumber daya air</h5>
                                <span class="h2 font-weight-bold mb-0 text-white">New List</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                    <img src="../images/clipboard.png" width="50">
                                </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                            <a href="{{URL::to('admin/sumberdayaair/newlist')}}" class="btn btn-md btn-default">Click Here</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card bg-gradient-danger border-0">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted text-white">Sumber Daya Air</h5>
                                    <span class="h2 font-weight-bold mb-0 text-white">Edit List</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                            <img src="../images/copywriter.png" width="50">
                                    </div>
                                </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                                <a href="{{URL::to('admin/sumberdayaair/publish')}}" class="btn btn-md btn-default">Click Here</a>
                        </p>
                    </div>
                </div>
            </div>
          </div>
          <div class="card bg-gradient-default text-white">
                <div class="card-body">
                    <form>
                        <div class="pl-lg-4">
                            <div class="form-group">
                                <div class="custom-control form-control-lg custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Unpublish file</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control form-control-lg custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                                    <label class="custom-control-label" for="customCheck2">Publish file</label>
                                </div>
                            </div>
                        </div>
                    </form>
                    <strong>Check your choose before click edit</strong> 
                </div>
          </div>
    </div>
</div>



@endsection
<style>

</style>