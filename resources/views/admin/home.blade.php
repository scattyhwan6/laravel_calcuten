@extends('layouts.admin')


@section('content')
<div class="container mt-6">
    <h2 class="display-4">Hi calculaten tim,</h2>
    <p>Pahami fungsi admin di bawah ini sebelum anda memulai </p>

    <h5 class="lead">Admin Editor</h5>
    <p>anda hanya dapat membuka page sumber daya air, page cipta karya, 
        serta melakukan editing bidang pekerjaan di page tersebut, mohon informasi ke super admin anda apabila melakukan editing. </p>
    
    <h5 class="lead">Admin User</h5>
    <p>anda hanya dapat membuka page user membership, untuk membuat laporan financial dan komunikasi dengan user.
        Terkait kendala user yang membutuhkan akses lain selain yang anda miliki, mohon hubungi super admin. </p>

    <h5 class="lead">Super admin</h5>
    <p>Rambang Basari <span>id@email.com</span></p>
    <p>Rosi Rosalinda <span>id@email.com</span></p>
    <p>Popy Patricia <span>id@email.com</span></p>
</div>
<div class="container mt-6">
    <div class="row col">
        <div class="form-group" style="margin-right:20px">
            <button type="button" class="btn btn-danger">Know the rule !</button><br>
            <span class="font-weight-bold">Peraturan admin</span>
        </div>
        <div class="form-group text-center" style="margin-right:20px">
            <a href="">
                <img src="../../images/email.png" width="40">
            </a><br>
            <span class="font-weight-bold">Kontak Team IT</span>
        </div>
        <div class="form-group text-center" style="margin-right:20px">
             <a href="">
                <img src="../../images/email.png" width="40">
            </a><br>
            <span class="font-weight-bold">Kontak Admin Manager</span>
        </div>
    </div>
</div>



@endsection