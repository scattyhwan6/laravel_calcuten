@extends('layouts.admin')


@section('content')
<div class="header bg-default pb-6">
      <div class="container">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h1 class=" text-white d-inline-block mb-0">Publish File Cipta Karya</h1><br>
              <span class="text-white btn btn-sm btn-default mr-4">Cipta Karya - Publish File</span> 
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">Home</a>
              <a href="#" class="btn btn-sm btn-neutral">Back</a>
              <br>

            </div>
          </div>
        </div>
      </div>
</div>
<div class="container mt--6">
    <div class="row">
        <div class="col-xl-10">
            <div class="card">
                <div class="table-scroll">
                    <ul class="list-group list-group-flush list my--3">
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                  <a href="{{URL::to('admin/sumberdayaair/edit')}}">1 m2 pembersihan dan stripping kosekan</a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/sumberdayaair/edit')}}">1 m2 tebas tebang nerupa memotong dan
                                  membersihkanloksi dari tanaman / tumbuhan diameter < 15cm
                                </a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                  <a href="{{URL::to('admin/sumberdayaair/edit')}}">cabut 1 tunggul pohon tanaman keras diameter > 15cm dan 
                                    membuang sisa tunggul dan akar-akarnya</a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/sumberdayaair/edit')}}">1 m2 uitset trase saluran</a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/sumberdayaair/edit')}}">Pasang 1m profil melintang galian tanah</a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/sumberdayaair/edit')}}">1 m2 galian tanah biasa sedalam < 1m</a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                  <a href="{{URL::to('admin/sumberdayaair/edit')}}">1 m2 pembersihan dan stripping kosekan</a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                              <a href="{{URL::to('admin/sumberdayaair/edit')}}">1 m2 tebas tebang nerupa memotong dan
                                membersihkanloksi dari tanaman / tumbuhan diameter < 15cm
                              </a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/sumberdayaair/edit')}}">cabut 1 tunggul pohon tanaman keras diameter > 
                                  15cm dan membuang sisa tunggul dan akar-akarnya</a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                  <a href="{{URL::to('admin/sumberdayaair/edit')}}">1 m2 uitset trase saluran</a>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                  <a href="{{URL::to('admin/sumberdayaair/edit')}}">Pasang 1m profil melintang galian tanah</a>
                            </div>
                        </li>
                         <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                   <a href="{{URL::to('admin/sumberdayaair/edit')}}">Pasang 1m profil melintang galian tanah</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
   
</div>

@endsection
<style>
.list-group li:nth-of-type(odd) {
    background-color: #7AC29A;
    padding: 10px !important;
}
.list-group li:nth-of-type(even) {
    background-color: #F3BB45;
    padding: 10px !important;
}
.list-group li > div a {
  color:white;
  font-size: 14px;
  padding: 10px;
}
</style>