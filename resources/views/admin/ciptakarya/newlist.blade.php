@extends('layouts.admin')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;">
    <div class="header bg-gradient-default pb-6">
          <div class="container">
            <div class="header-body">
              <div class="row align-items-center py-4">
                  <div class="col-lg-6 col-7">
                    <h1 class=" text-white d-inline-block mb-0">Publish File Cipta Karya</h1><br>
                    <span class="text-white btn btn-sm btn-default mr-4">Cipta Karya - Newlist File</span>  
                  </div>
                  <div class="col-lg-6 col-5 text-right">
                    <a href="#" class="btn btn-sm btn-neutral">Home</a>
                    <a href="#" class="btn btn-sm btn-neutral">Back</a>
                  </div>
              </div>
            </div>
          </div>
    </div>
    <div class="container mt--6">
        <div class="row justify-content-center mb-5">
              <div class="col-md-12">
                  <div class="table-responsive table-stripe">
                      @include('admin.table.ciptakarya')
                  </div>
              </div>
             
        </div>
        <div class="row">
            <div class="col-md-12 mt-5 text-right mb-3">
                 @include('admin.inc.button')
            </div>
        </div>
    </div>
</div>




@endsection
