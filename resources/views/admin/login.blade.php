@extends('layouts.admin')


@section('content')
<div class="header page-header py-7 py-lg-8 pt-lg-9"style="background-image: url(../../images/admin-bg.jpg);
    transform: translate3d(0px, 0px, 0px);
    color: black;"  >
        <div class="page-header-image"></div>
        <div class="filter"></div>
        <div class="content-center pb-5">
            <div class="container" style="z-index:10000">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-7">
                            <h1 _ngcontent-c2="" class="title">Your sign in is required</h1>
                            <div class="card bg-secondary border-0 mb-0">
                                <div class="card-body px-lg-5 py-lg-5">
                                    <form role="form">
                                        <div class="form-group mb-3">
                                        <div class="input-group input-group-merge input-group-alternative">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Email" type="email">
                                        </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="input-group input-group-merge input-group-alternative">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Password" type="password">
                                        </div>
                                        </div>
                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                        <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                                        <label class="custom-control-label" for=" customCheckLogin">
                                            <span class="text-muted">Remember me</span>
                                        </label>
                                        </div>
                                        <div class="text-center">
                                        <button type="button" class="btn btn-primary my-4">Sign in</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-6">
                                <a href="#" class="text-light"><small>Forgot password?</small></a>
                                </div>
                                <div class="col-6 text-right">
                                <a href="#" class="text-light"><small>Create new account</small></a>
                                </div>
                            </div>  
                        </div>
                    </div>
            </div>
        </div>

</div>
@endsection
<style>
.title {
    margin-top: 30px;
    margin-bottom: 25px;
    min-height: 32px;
    font-family: Montserrat,Helvetica,Arial,sans-serif;
    color:white;
    font-weight: 400;
}
.page-header {
    background-color: #b2afab;
    background-position: center center;
    background-size: cover;
    min-height: 100vh;
    max-height: 999px;
    overflow: hidden;
    position: relative;
    width: 100%;
    z-index: 1;
}
.page-header-image {
    position: absolute;
    background-size: cover;
    background-position: center center;
    width: 100%;
    height: 100%;
    z-index: -1;
}
.filter, .tagsinput .tag, .tagsinput-remove-link, [data-toggle=collapse] i {
    transition: 150ms linear;
}
.page-header .filter::after {
    background-color: rgba(0,0,0,.5);
    content: "";
    display: block;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 2;
}
.content-center {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 2;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
    text-align: center;
    color: #fff;
    width: 100%;
}

</style>