@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Page New Admin</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">New Admin</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Admin Data</li>
                </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Home</a>
            <a href="#" class="btn btn-sm btn-neutral">Back</a>
            <br>
        </div>
        </div>
    </div>
    </div>
</div>

<div class="container mt--6">
    <div class="card">
        <!-- Light table -->
        <div class="table-responsive">
            <table class="table align-items-center table-striped">
                <thead class="thead-light">
                <tr>
                    <th>Nama Admin</th>
                    <th>Nama ID Admin</th>
                    <th>Password</th>
                    <th>Nomor Admin</th>
                    <th>Divisi</th>
                    <th>Jenis Admin</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mulyadi
                        </td>
                        <td>editor 101</td>
                        <td>
                            Tam1o01
                        </td>
                        <td class="table-actions">
                            60001
                        </td>
                        <td>SDA Database</td>
                        <td>Editor</td>
                        <td class="table-actions">
                            <a href="#!" class="table-action table-action-delete" data-toggle="modal" data-target="#admin-info">
                                <i class="fa fa-folder"></i>
                            </a>
                            <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-edit"></i>
                            </a>
                             <a href="#!" class="table-action table-action-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

{{-- Modal --}}
<div class="row">
    <div class="col-md-4">
        <div class="modal fade" id="admin-info" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
            <div class="modal-dialog modal modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header header bg-gradient-success">
                        <h3 class="modal-title display-3 text-white" id="modal-title-default">Admin Database </h3>
                        <div class="col-auto">
                            <span class="badge badge-lg badge-success">Active</span>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-0">
                            <p class="col-md-4 font-weight-bold">Name</p>
                            <div class="col-md-8">
                                <p class="">: Ir.Mulyadi Sutedja</p>
                            </div>
                        </div>
                        <div class="row mb-0">
                            <p class="col-md-4 font-weight-bold">ID Admin</p>
                            <div class="col-md-8">
                                <p class="">: editor101</p>
                            </div>
                        </div>
                        <div class="row mb-0">
                            <p class="col-md-4 font-weight-bold">Password</p>
                            <div class="col-md-8">
                                <p class="">: TAM105</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-4 font-weight-bold">Jenis Admin</p>
                            <div class="col-md-8">
                                <p class="">: Editor</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-4 font-weight-bold">Nom. karyawan</p>
                            <div class="col-md-8">
                                <p class="">: CAD980 - 60001</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-4 font-weight-bold">Divisi</p>
                            <div class="col-md-8">
                                <p class="">: SDA Database</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-4 font-weight-bold">Aktifasi</p>
                            <div class="col-md-8">
                                <p class="">: 01 januari 2019</p>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <p class="col-md-4 font-weight-bold">Akhir Aktifasi</p>
                            <div class="col-md-8">
                                <p class="">: 01 januari 2020</p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <b class="custom-form">admin status :</b>
                        <label class="custom-toggle">
                            <input type="checkbox" checked="">
                            <span class="custom-toggle-slider rounded-circle" data-label-off="inactive" data-label-on="active"></span>
                        </label>
                        <button type="button" class="btn btn-icon-only  rounded-circle">
                            <i class="fa fa-share-square" style="font-size:20px"></i>
                        </button>
                        <button type="button" class="btn btn-icon-only rounded-circle">
                            <i class="fa fa-download" style="font-size:20px"></i>
                        </button>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<style>
.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(11, 154, 255, 0.3) !important;
    color: #484141 !important;
}
</style>