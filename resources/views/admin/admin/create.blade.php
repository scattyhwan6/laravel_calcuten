@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
        <div class="header-body">
            <div class="row">
                <div class="col-lg-6 col-7 py-4">
                    <h6 class="h2 text-white d-inline-block mb-0">Create Admin</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Page New admin - Create admin</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-sm-12 align-items-center text-right mb-3">
                    <a href="#" class="btn btn-sm btn-neutral">Home</a>
                    <a href="#" class="btn btn-sm btn-neutral">Back</a>
                </div>
            </div>
           
        </div>
    </div>
</div>
<div class="container mt--6">
    <div class="row">
        <div class="col-xl-8 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0">Create Admin</h3>
                </div>
                <div class="card-body">
                    <form>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Nama ID Admin</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="example-text-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Password</label>
                            <div class="col-md-8">
                                <input class="form-control" type="password" id="example-text-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Nama ID Admin</label>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="example-date-input" class="form-control-label">Dari
                                        <i class="fa fa-calendar"></i>
                                    </label><br>
                                    <input class="form-control" type="date" value="2018-11-23" id="example-date-input">
                                </div>
                                <div class="form-group">
                                    <label for="example-date-input" class="form-control-label">Sampai dengan
                                         <i class="fa fa-calendar"></i>
                                    </label><br>
                                    <input class="form-control" type="date" value="2018-11-23" id="example-date-input">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Nomor Admin</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="example-text-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Jenis Admin</label>
                            <div class="col-md-8">
                                <div class="custom-control custom-checkbox mb-3">
                                    <input class="custom-control-input" id="admin1" type="checkbox">
                                    <label class="custom-control-label" for="admin1">Admin Editor</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-3">
                                    <input class="custom-control-input" id="admin2" type="checkbox">
                                    <label class="custom-control-label" for="admin2">Admin User</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-3">
                                    <input class="custom-control-input" id="admin3" type="checkbox">
                                    <label class="custom-control-label" for="admin3">Admin Super</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Nama Lengkap</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="example-text-input">
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Divisi</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="example-text-input">
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Nomor Karyawan</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="example-text-input">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xl-4 order-xl-2">
            <div class="card">
                <div class="card-body">
                    <div class="picture-container">
                        <div class="picture">
                            <img src="https://lh3.googleusercontent.com/LfmMVU71g-HKXTCP_QWlDOemmWg4Dn1rJjxeEsZKMNaQprgunDTtEuzmcwUBgupKQVTuP0vczT9bH32ywaF7h68mF-osUSBAeM6MxyhvJhG6HKZMTYjgEv3WkWCfLB7czfODidNQPdja99HMb4qhCY1uFS8X0OQOVGeuhdHy8ln7eyr-6MnkCcy64wl6S_S6ep9j7aJIIopZ9wxk7Iqm-gFjmBtg6KJVkBD0IA6BnS-XlIVpbqL5LYi62elCrbDgiaD6Oe8uluucbYeL1i9kgr4c1b_NBSNe6zFwj7vrju4Zdbax-GPHmiuirf2h86eKdRl7A5h8PXGrCDNIYMID-J7_KuHKqaM-I7W5yI00QDpG9x5q5xOQMgCy1bbu3St1paqt9KHrvNS_SCx-QJgBTOIWW6T0DHVlvV_9YF5UZpN7aV5a79xvN1Gdrc7spvSs82v6gta8AJHCgzNSWQw5QUR8EN_-cTPF6S-vifLa2KtRdRAV7q-CQvhMrbBCaEYY73bQcPZFd9XE7HIbHXwXYA=s200-no" class="picture-src" id="wizardPicturePreview" title="">
                            <input type="file" id="wizard-picture" class="">
                        </div>
                        <h6 class="">Choose Picture</h6>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <button type="button" class="btn btn-primary mb-3">Back</button>
                    <button class="btn btn-icon btn-danger mb-3" type="button">
                        <span class="btn-inner--icon"><i class="fa fa-remove"></i></span>
                        <span class="btn-inner--text">Cancel</span>
                    </button>
                    <button type="button" class="btn btn-default btn-block">Save and create new</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style>
/*Profile Pic Start*/
.picture-container{
    position: relative;
    cursor: pointer;
    text-align: center;
}
.picture{
    width: 150px;
    height: 150px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
    border-radius: 50%;
    margin: 0px auto;
    overflow: hidden;
    transition: all 0.2s;
    -webkit-transition: all 0.2s;
}
.picture:hover{
    border-color: #2ca8ff;
}
.content.ct-wizard-green .picture:hover{
    border-color: #05ae0e;
}
.content.ct-wizard-blue .picture:hover{
    border-color: #3472f7;
}
.content.ct-wizard-orange .picture:hover{
    border-color: #ff9500;
}
.content.ct-wizard-red .picture:hover{
    border-color: #ff3b30;
}
.picture input[type="file"] {
    cursor: pointer;
    display: block;
    height: 100%;
    left: 0;
    opacity: 0 !important;
    position: absolute;
    top: 0;
    width: 100%;
}

.picture-src{
    width: 100%;
    
}
/*Profile Pic End*/
</style>
<script>
$(document).ready(function(){
// Prepare the preview for profile picture
    $("#wizard-picture").change(function(){
        readURL(this);
    });
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}
</script>