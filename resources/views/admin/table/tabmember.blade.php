<meta charset="utf-8">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://www.w3schools.com/lib/w3.js"></script>
<script type="text/javascript" src="{{asset('js/footable.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="/css/table-custom.css">

{{-- <table class="tableizer-table table-hover">
    <tr class="header">
        <th style="cursor:pointer">User Number</th>
        <th onclick="w3.sortHTML('#myTable', '.item', 'td:nth-child(1)')" style="cursor:pointer">
            Nama <i class="fa fa-sort" style="font-size:13px;"></i>
        </th>
        <th style="cursor:pointer">Invoice Number</th>
        <th onclick="w3.sortHTML('#myTable', '.item', 'td:nth-child(2)')" style="cursor:pointer">
            Status <i class="fa fa-sort" style="font-size:13px;"></i>
        </th>
        <th onclick="w3.sortHTML('#myTable', '.item', 'td:nth-child(3)')" style="cursor:pointer">
            Membership <i class="fa fa-sort" style="font-size:13px;"></i>
        </th>
        <th style="cursor:pointer">Billing</th>
    </tr>
    <tbody id="myTable">
        <tr >
            <td>1212323</td>
            <td class="item">Ir. Mulyadi Sutedja</td>
            <td>
                <a href="#!">298925102</a>
            </td>
            <td>Paid</td>
            <td>Business</td>
            <td>100.000</td>
        </tr>
        <tr>
            <td>43434344</td>
            <td class="item">Syamsul Bahri</td>
            <td>
                <a href="#!">723782892</a>
            </td>
            <td>UnPaid</td>
            <td>Enterprise</td>
            <td>100.000</td>
        </tr>
        <tr>
            <td>23283823</td>
            <td class="item">Rosi Rosalinda</td>
            <td>
                <a href="#!">198929209</a>
            </td>
            <td>Paid</td>
            <td>Starter</td>
            <td>100.000</td>
        </tr>
    </tbody>
</table>  --}}

  


<table class="order-table tableizer-table table-hover">
    <thead>
        <tr>
            <th style="cursor:pointer">User Number</th>
            <th style="cursor:pointer">
                Nama <i class="fa fa-sort" style="font-size:13px;"></i>
            </th>
            <th style="cursor:pointer">Invoice Number</th>
            <th style="cursor:pointer">
                Status <i class="fa fa-sort" style="font-size:13px;"></i>
            </th>
            <th style="cursor:pointer">
                Membership <i class="fa fa-sort" style="font-size:13px;"></i>
            </th>
            <th style="cursor:pointer">Billing</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>23283823</td>
            <td>Rosi Rosalinda</td>
            <td>
                <a href="#!" data-toggle="modal" data-target="#invoicemodal">198929209</a>
            </td>
            <td>Paid</td>
            <td>Starter</td>
            <td>100.000</td>
        </tr>
        <tr >
            <td>1212323</td>
            <td>Ir. Mulyadi Sutedja</td>
            <td>
                <a href="#!" data-toggle="modal" data-target="#invoicemodal">298925102</a>
            </td>
            <td>Paid</td>
            <td>Business</td>
            <td>100.000</td>
        </tr>
        <tr>
            <td>43434344</td>
            <td>Syamsul Bahri</td>
            <td>
                <a href="#!" data-toggle="modal" data-target="#invoicemodal">723782892</a>
            </td>
            <td class="unpaid">Unpaid</td>
            <td>Enterprise</td>
            <td>100.000</td>
        </tr>
        <tr>
            <td>23283823</td>
            <td>Rosi Rosalinda</td>
            <td>
                <a href="#!" data-toggle="modal" data-target="#invoicemodal">198929209</a>
            </td>
            <td>Paid</td>
            <td>Starter</td>
            <td>100.000</td>
        </tr>
        <tr >
            <td>1212323</td>
            <td>Ir. Mulyadi Sutedja</td>
            <td>
                <a href="#!" data-toggle="modal" data-target="#invoicemodal">298925102</a>
            </td>
            <td>Paid</td>
            <td>Business</td>
            <td>100.000</td>
        </tr>
        <tr>
            <td>43434344</td>
            <td>Syamsul Bahri</td>
            <td>
                <a href="#!" data-toggle="modal" data-target="#invoicemodal">723782892</a>
            </td>
            <td class="unpaid">Unpaid</td>
            <td>Enterprise</td>
            <td>100.000</td>
        </tr>
    </tbody>
</table>

<style>
.unpaid{
    color: red;
}
th {
    padding: 10px !important;
}
</style>
{{-- <script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("mylist");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "None";
      }
    }       
  }
}
$("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
</script> --}}
<script>
(function(document) {
	'use strict';

	var LightTableFilter = (function(Arr) {

		var _input;
    var _select;

		function _onInputEvent(e) {
			_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) {
				Arr.forEach.call(table.tBodies, function(tbody) {
					Arr.forEach.call(tbody.rows, _filter);
				});
			});
		}
    
		function _onSelectEvent(e) {
			_select = e.target;
			var tables = document.getElementsByClassName(_select.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) {
				Arr.forEach.call(table.tBodies, function(tbody) {
					Arr.forEach.call(tbody.rows, _filterSelect);
				});
			});
		}

		function _filter(row) {
      
			var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';

		}
    
		function _filterSelect(row) {
     
			var text_select = row.textContent.toLowerCase(), val_select = _select.options[_select.selectedIndex].value.toLowerCase();
			row.style.display = text_select.indexOf(val_select) === -1 ? 'none' : 'table-row';

		}

		return {
			init: function() {
				var inputs = document.getElementsByClassName('light-table-filter');
				var selects = document.getElementsByClassName('select-table-filter');
				Arr.forEach.call(inputs, function(input) {
					input.oninput = _onInputEvent;
				});
				Arr.forEach.call(selects, function(select) {
         select.onchange  = _onSelectEvent;
				});
			}
		};
	})(Array.prototype);

	document.addEventListener('readystatechange', function() {
		if (document.readyState === 'complete') {
			LightTableFilter.init();
		}
	});

    

})(document);
</script>