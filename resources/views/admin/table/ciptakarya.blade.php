

<link rel="stylesheet" type="text/css" href="/css/table-custom.css">

<table class="tableizer-table table-hover" id="sumberdaya">
    <thead>
        <tr class="tableizer-firstrow">
            <th>
                <div class="relative">
                    <span class="colHeader">No</span>
                </div>
            </th>
            <th>
                <div class="relative">
                    <span class="colHeader">Uraian</span>
                </div>
            </th>
            <th>
                <div class="relative">
                    <span class="colHeader">SAT</span>
                </div>
            </th>
            <th>
                <div class="relative">
                    <span class="colHeader">Koef</span>
                </div>
            </th>
            <th>
                <div class="relative">
                    <span class="colHeader">Harga Satuan</span>
                </div>
            </th>
            <th>
                <div class="relative">
                    <span class="colHeader">Jumlah <br>Harga (Rp)</span>
                </div>
            </th>
            <th>
                <div class="relative">
                    <span class="colHeader">Keterangan</span>
                </div>
            </th>
        </tr>
            <tr>
            <td>
                <div class="relative"><span class="rowHeader">1</span></div>
            </td>
            <td>
                <div class="relative"><span class="rowHeader">2</span></div>
            </td>
            <td>
                <div class="relative"><span class="rowHeader">3</span></div>
            </td>
            <td>
                <div class="relative"><span class="rowHeader">4</span></div>
            </td>
            <td>
                <div class="relative"><span class="rowHeader">5</span></div>
            </td>
            <td>
                <div class="relative"><span class="rowHeader">6</span></div>
            </td>
            <td>
                <div class="relative"><span class="rowHeader">7</span></div>
            </td>
        </tr>
    </thead>
    <tbody>
        {{-- Section_A --}}
        <tr>
            <td>A</td>
            <td>Tenaga</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="txtMult">
            <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Edit
                </button>
            </td>
            <td contenteditable="true">Input By Admin</td>
            <td contenteditable="true">by admin</td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom val1 text-center"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom val2 text-center"  style="">0.00</textarea>
                </div>
                {{-- <input name="txtEmmail" class="val2"/> --}}
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom multTotal text-center" disabled style="">0.00</textarea>
                </div>
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
        <tr class="txtMult">
            <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Edit
                </button>
            </td>
            <td contenteditable="true">Input By Admin</td>
            <td contenteditable="true">by admin</td>
            <td>
                    <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom val1 text-center"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom val2 text-center"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom multTotal text-center" disabled style="">0.00</textarea>
                </div>
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
         <tr class="txtMult">
            <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Edit
                </button>
            </td>
            <td contenteditable="true">Input By Admin</td>
            <td contenteditable="true">by admin</td>
            <td>
                    <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom val1 text-center"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom val2 text-center"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom multTotal text-center" disabled style="">0.00</textarea>
                </div>
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <span style="font-weight:bold">Jumlah tenaga kerja</span>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">
                <div class="handsontableInputHolder">
                    <textarea id="grandTotal" tabindex="-1" class="handsontableInput 
                    area-custom text-center text-bold" disabled style="">0.00</textarea>
                </div>
                {{-- <span id="grandTotal">0.00</span> --}}
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
        {{-- Section_B --}}
        <tr>
            <td>B</td>
            <td>Tenaga</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="txtBahan">
            <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Edit
                </button>
            </td>
            <td contenteditable="true">Input By Admin</td>
            <td contenteditable="true">by admin</td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  bhnKoef text-center text-bold"  style="">0.00</textarea>
                </div>
                
            </td>
            <td>
                    <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  bhnPrice text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  bahanTotal text-center text-bold" disabled style="">0.00</textarea>
                </div>
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
        <tr class="txtBahan">
            <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Edit
                </button>
            </td>
            <td contenteditable="true">Input By Admin</td>
            <td contenteditable="true">by admin</td>
            <td>
                    <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  bhnKoef text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  bhnPrice text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  bahanTotal text-center text-bold" disabled  style="">0.00</textarea>
                </div>
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td> <span style="font-weight:bold">Jumlah Harga Bahan</span></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">
                    <div  class="handsontableInputHolder">
                    <textarea id="grandTotal_Bahan" tabindex="-1" class="handsontableInput 
                    area-custom text-right text-bold" disabled  style="">0.00</textarea>
                </div>
                
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
        {{-- Section_C --}}
        <tr>
            <td>C</td>
            <td>Peralatan</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="txtAlat">
            <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Edit
                </button>
            </td>
            <td contenteditable="true">Input By Admin</td>
            <td contenteditable="true">by admin</td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom alatKoef text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom alatPrice text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom alatTotal text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
        <tr class="txtAlat">
            <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Edit
                </button>
            </td>
            <td contenteditable="true">Input By Admin</td>
            <td contenteditable="true">by admin</td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom alatKoef text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom alatPrice text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                    <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom alatTotal text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td> <span style="font-weight:bold">Jumlah Harga Alat</span></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">
                    <div class="handsontableInputHolder">
                    <textarea id="grandTotal_alat" tabindex="-1" class="handsontableInput 
                    area-custom text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td contenteditable="true">by admin</td>
        </tr>
            {{-- Section_D --}}
        <tr class="txtGrandtotal">
            <td>D</td>
            <td>Jumlah (A + B + C)</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td><span id="totalAll" class="handsontableInput area-custom text-bold">0.00</span></td>
            <td contenteditable="true">by Admin</td> 
        </tr>
            {{-- Section_E --}}
        <tr class="txtGrandDisc">
            <td>E</td>
            <td>Overhead dan profit 15%</td>
            <td>&nbsp;</td>
            <td>
                <input type="number" class="handsontableInput area-custom" id="chDiscount" value="10">
            </td>
            <td></td>
            <td><span id="result" class="handsontableInput area-custom text-bold">0.00</span></td>
            <td>by Admin</td> 
        </tr>
        {{-- Section_F --}}
        <tr class="txtGrandDisc">
            <td>F</td>
            <td>Harga satuan Pekerjaan (D + E)</td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
            <td><span id="hargaresult" class="handsontableInput area-custom text-bold">0.00</span></td>
            <td contenteditable="true">by Admin</td> 
        </tr>
    </tbody>
</table> 

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Input Tenaga</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        <div class="modal-body">
            <form role="form">
                <div class="form-group mb-3">
                    <div class="input-group input-group-merge input-group-alternative">
                        <input type="text" class="form-control form-control-muted" placeholder="Uraian">
                    </div>
                </div>  
                <div class="form-group mb-3">
                    <div class="input-group input-group-merge input-group-alternative">
                        <input type="text" class="form-control form-control-muted" placeholder="Sat">
                    </div>
                </div> 
                <div class="form-group mb-3">
                    <div class="input-group input-group-merge input-group-alternative">
                        <input type="text" class="form-control form-control-muted" placeholder="Koef">
                    </div>
                </div> 
                <div class="form-group mb-3">
                    <div class="input-group input-group-merge input-group-alternative">
                        <input type="text" class="form-control form-control-muted" placeholder="Keterangan">
                    </div>
                </div>       
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Submit</button>
            <button type="button" class="btn btn-primary">Reset</button>
            <button type="button" class="btn btn-primary">Close</button>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="{{asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/function-table.js') }}"></script>
<script type="text/javascript" src="{{asset('js/jspdf.min.js') }}"></script>


