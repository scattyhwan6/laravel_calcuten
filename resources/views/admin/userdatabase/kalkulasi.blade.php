@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h2 class=" text-white d-inline-block mb-3">Nama User</h2><br>
            <h1 class=" text-white d-inline-block mb-0">Folder Kalkulasi Proyek</h1><br>
            <span class="d-inline-block mb-3 btn btn-default">Open Folder - Folder Kalkulasi Proyek</span>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Home</a>
            <a href="#" class="btn btn-sm btn-neutral">Back</a>
            <br>

        </div>
        </div>
    </div>
    </div>
</div>
<div class="container mt--6">
    <div class="row">
        <div class="col-xl-8">
            <div class="card">
                <div class="table-scroll">
                    <ul class="list-group list-group-flush list my--3">
                        <li class="bg-gradient-success list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/userdatabase/openkalkulasi')}}">New Folder</a>
                            </div>
                        </li>
                        <li class="bg-gradient-success list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/userdatabase/openkalkulasi')}}">
                                    New Folder
                                </a>
                            </div>
                        </li>
                        <li class="bg-gradient-success list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/userdatabase/openkalkulasi')}}">New Folder</a>
                            </div>
                        </li>
                        <li class="bg-gradient-success list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/userdatabase/openkalkulasi')}}">
                                    New Folder
                                </a>
                            </div>
                        </li>
                        <li class="bg-gradient-success list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/userdatabase/openkalkulasi')}}">New Folder</a>
                            </div>
                        </li>
                        <li class="bg-gradient-success list-group-item px-0">
                            <div class="row align-items-center">
                                <a href="{{URL::to('admin/userdatabase/openkalkulasi')}}">
                                    New Folder
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>|
</div>

@endsection
<style>
.list-group li:nth-of-type(odd) {
    background-color: #7AC29A;
    padding: 10px !important;
}
.list-group li:nth-of-type(even) {
    background-color: #F3BB45;
    padding: 10px !important;
}
.list-group li > div a {
    color:white;
    font-size: 14px;
    padding: 10px;
}
.bg-green {
    background-color: #4fd69c !important;
}
</style>