@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h1 class="display-3 text-white">User name</h1>
            <span class="text-white btn btn-sm btn-default mr-4 mb-3">Folder Sumber Daya Air</span> <br>
            <h4 class=" text-white d-inline-block mb-3">Pembuatan 1 m2 bekisting untuk balok beton pencetak</h4><br>       
        </div>
        <div class="col-lg-6 col-5 text-right"></div>
        </div>
    </div>
    </div>
</div>
<div class="container mt--6">
    <div class="row justify-content-center mb-5">
              <div class="col-md-12">
                  <div class="table-responsive table-stripe">
                      @include('admin.table.tableusersda')
                  </div>
              </div>
             
        </div>
        <div class="row">
            <div class="col-md-12 mt-5 text-right mb-3">
                 @include('admin.inc.buttonuser')
            </div>
        </div>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection
