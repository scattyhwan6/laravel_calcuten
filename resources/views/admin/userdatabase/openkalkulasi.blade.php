@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
                <h1 class="display-3 text-white">User name</h1>
                <span class="text-white btn btn-sm btn-default mr-4 mb-3">Kalkulasi Proyek</span> <br>
                <h4 class=" text-white d-inline-block mb-3">Proyek pembuatan rumah dinas</h4><br>       
            </div>
            <div class="col-lg-6 col-5 text-right"></div>
        </div>
    </div>
    </div>
</div>
<div class="container mt--6">
    <div class="row">
        <div class="col-md-8">
           @include('admin.table.tabkalkulasi')
        </div>
        <div class="col-md-4">
            @include('admin.inc.buttonuser')
        </div>
    </div>
</div>

@endsection
<style>
.list-group li:nth-of-type(odd) {
    background-color: #7AC29A;
    padding: 10px !important;
}
.list-group li:nth-of-type(even) {
    background-color: #F3BB45;
    padding: 10px !important;
}
.list-group li > div a {
    color:white;
    font-size: 14px;
    padding: 10px;
}
.bg-green {
    background-color: #4fd69c !important;
}
</style>