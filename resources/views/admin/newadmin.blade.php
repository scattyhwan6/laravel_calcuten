@extends('layouts.admin')


@section('content')

<div class="header d-flex align-items-center" style="min-height: 500px; background-image: url(../../images/writing.jpg); background-size: cover; background-position: center top;">   
    <div class="container">
        <span class="mask bg-gradient-default opacity-8"></span>
        <!-- Header container -->
        <div class="container-fluid d-flex align-items-center">
            <div class="row">
                <div class="col-lg-7 col-md-10">
                    <h1 class="display-2 text-white mb-5">New Admin</h1>
                </div>
            </div>
        </div>
    </div>   
</div>
<div class="container mt--6">
    <div class="col-xl-8 order-xl-1">
          <div class="row">
            <div class="col-lg-6">
                 <div class="card bg-gradient-default">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                 <div class="my-4">
                                    <span class="h6 surtitle text-light">
                                        New Admin
                                    </span>
                                    <div class="h1 text-white">Create new admin</div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                    <img src="../images/pay.png" width="55">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <button type="button" class="btn btn-md btn-default btn-icon">
                                <span class="btn-inner--icon"><i class="fa fa-plus"></i></span>
                                <a href="{{URL::to('admin/create')}}" class="btn-inner--text text-white">add</a>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card bg-gradient-success">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                 <div class="my-4">
                                    <span class="h6 surtitle text-light">
                                        New Admin
                                    </span>
                                    <div class="h1 text-white">Admin Data</div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                    <img src="../images/pay.png" width="55">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <button type="button" class="btn btn-md btn-default btn-icon">
                                <span class="btn-inner--icon"><i class="fa fa-plus"></i></span>
                                <a href="{{URL::to('admin/admindata')}}" class="btn-inner--text text-white">see detail</a>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
</div>



@endsection
<style>
p {
    font-size:1.5em!important;
}
.btn{
    font-size:14px!important;
}
.custom-control-label::before, 
.custom-control-label::after {
top: 1rem;
width: 2.25rem !important;
height: 2.25rem !important;
}
</style>