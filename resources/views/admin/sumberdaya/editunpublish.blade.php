@extends('layouts.admin')


@section('content')
<div class="header bg-gradient-default pb-6">
    <div class="container">
        <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
                <h1 class=" text-white d-inline-block mb-0">Pembuatan 1 m2 bekisting
                    untuk balok beton pencetak (10-12 kali pakai)</h1><br>  
                <span class="text-white btn btn-sm btn-danger mr-4">SDA - Unpublish File</span>        
            </div>
            <div class="col-lg-6 col-5 text-right">
                <a href="#" class="btn btn-sm btn-neutral">Home</a>
                <a href="#" class="btn btn-sm btn-neutral">Back</a>
                <br>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="container mt--6">
        <div class="row ">
              <div class="col-md-12 mb-5">
                  <div class="table-responsive table-stripe">
                      @include('admin.table.ciptakarya')
                  </div>
              </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-5 text-right mb-3">
                 @include('admin.inc.button2')
            </div>
        </div>
    </div>
</div>



@endsection
