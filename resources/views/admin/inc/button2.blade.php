
<link rel="stylesheet" type="text/css" href="/css/sweetalert.min.css">
    <div class="ml-auto mr-auto text-center" style="position:absolute;bottom:0">
        <div class="links">
            <ul class="uppercase-links">
                <li>
                    <a href="#!" class="text-white">
                        <a class="" href="" data-toggle="modal" data-target="#deletedModal">
                            <img src="../../images/trash.png" width=40>
                        </a><br>
                        Deleted
                    </a>
                </li>
                <li>
                    <a href="#!" id="export"  class="text-grey" data-toggle="modal" data-target="#publishModal">
                        <img src="../../images/paper-plane.png" width="40"><br>
                        Publish
                    </a>
                </li>
                <li>
                    <a href="#!" class="text-white">
                        <a class="" href="" data-toggle="modal" data-target="#saveModal">
                            <img src="../../images/floppy-disk.png" width=40>
                        </a><br>
                        Save
                    </a>
                </li>
                
            </ul>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="no-title modal-header no-border-header"><button class="close" type="button"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body text-center"><h5>Do you want to save this?</h5></div>
                <div class="modal-footer" style="border-top: 1px solid #ddd;
            padding:0;">
                    <div class="left-side">
                        <button class="btn btn-default btn-link" type="button">Never mind</button>
                    </div>
                    <div class="divider"></div>
                    <div class="right-side">
                        <button class="btn btn-danger btn-link" type="button">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="deletedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="no-title modal-header no-border-header"><button class="close" type="button"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body text-center">
                <div class="checked-icon">
                    <img src="../../images/warning.png" width="80">
                </div>
                <h5 class="title-modal">Do you want to deleted this?</h5></div>
            <div class="modal-footer text-center">
            <div class="swal2-actions" style="display: flex;">
                    <button type="button" class="swal2-confirm btn btn-danger" aria-label="">Yes, delete it!</button>
                    <button type="button" class="swal2-cancel btn btn-secondary" aria-label="" style="display: inline-block;">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="no-title modal-header no-border-header"><button class="close" type="button"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body text-center">
                    <div class="checked-icon">
                        <img src="../../images/checked.png" width="80">
                    </div>
                    <h2 class="title-modal text-center"  style="display:flex;">Success</h2>
                    <div id="swal2-content" style="display: block;">Your file has been publish..</div>
                </div>
                <div class="modal-footer text-center ml-auto mr-auto">
                <button type="button" class="swal2-confirm btn btn-success" aria-label="">OK</button>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript" src="{{asset('js/sweetalert.min.js') }}"></script>
<script>
function printData() {
        window.print()
}
</script>
<style>
.checked-icon {
    position: relative;
    justify-content: center;
    width: 5em;
    height: 5em;
    margin: 1.25em auto 1.875em;
    border: .25em solid transparent;
    border-radius: 50%;
    line-height: 5em;
    cursor: default;
    box-sizing: content-box;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    zoom: normal;
}
.title-modal {
    display: block;
    position: relative;
    max-width: 100%;
    margin: 0 0 .4em;
    padding: 0;
    color: #595959;
    font-size: 1.875em;
    font-weight: 600;
    text-align: center;
    text-transform: none;
    word-wrap: break-word;
}
.swal2-actions {
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
    margin: 1.25em auto 0;
    z-index: 1;
}
</style>