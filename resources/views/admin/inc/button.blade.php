<div class="ml-auto mr-auto text-center" style="position:absolute;bottom:0">
    <div class="links">
        <ul class="uppercase-links">
            <li>
                <a href="#!" id="export"  class="text-grey" onclick="javascript:demoFromHTML();">
                    <img src="../../images/plus.png" width="40"><br>
                    New List
                </a>
            </li>
            <li>
                <a href="#!" class="text-white">
                    <a class="" href="" data-toggle="modal" data-target="#saveModal">
                        <img src="../../images/floppy-disk.png" width=40>
                    </a><br>
                    Save
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="no-title modal-header no-border-header"><button class="close" type="button"><span aria-hidden="true">×</span></button></div>
        <div class="modal-body text-center"><h5>Do you want to save this?</h5></div>
        <div class="modal-footer" style="border-top: 1px solid #ddd;
    padding:0;">
            <div class="left-side">
                <button class="btn btn-default btn-link" type="button">Never mind</button>
            </div>
            <div class="divider"></div>
            <div class="right-side">
                <button class="btn btn-danger btn-link" type="button">Yes</button>
            </div>
        </div>
    </div>
  </div>
</div>


<style>
.modal-footer .btn-link {
    padding: 20px;
    width: 100%;
}
.modal-body {
    padding: 20px 50px;
    color: #000;
}
.modal-header.no-border-header {
    border-bottom: 0!important;
}
.modal-header {
    padding: 20px;
    text-align: center;
    display: block!important;
}
.modal-footer .divider {
    background-color: #ddd;
    display: inline-block;
    float: inherit;
    height: 63px;
    margin: 0 -3px;
    width: 1px;
}
.modal-footer {
    display: flex;
    align-items: center;
    justify-content: flex-end;
}
.modal-footer .left-side, .modal-footer .right-side {
    display: inline-block;
    text-align: center;
    width: 49%;
}
.modal-footer .btn-link {
    padding: 20px;
    width: 100%;
}
.btn-default,.btn-danger{
    border:0;
}
.btn-danger.btn-link{
    color:red;
}
 .btn.btn-link {
    background-color: transparent;
}
</style>
<script>
function printData() {
        window.print()
}
</script>