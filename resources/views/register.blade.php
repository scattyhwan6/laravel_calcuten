@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:100vh !important;z-index:1000;background:#3e3e3e;">
 <nav class="navbar navbar-expand-md bg-info">
	<div class="container">
		<a class="navbar-brand text-white">Register</a>
		<div class="col-md-6 col-sm-12 text-right">
			<ul class="navbar-nav mr-auto pull-right">
				<li class="nav-item active">
					<a  class="nav-link" href="#!">Home</a>
				</li>
				<li  class="nav-item">
					<a  class="nav-link" href="#!">Back</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<div class="container py-5 mt-6">
	<div class="row" style="margin-top:5%">
		<div class="col-md-8">
				<div class="card card-user">
					<div class="card-body">
						<form>
							<div class="row">
								<div class="col-md-4 pr-1">
									<div class="form-group">
										<label class="control-label">
											Please select
										</label>
										<select id="inputState" class="form-control">
											<option selected>Mr.</option>
											<option>Mrs.</option>
											<option>Ms.</option>
										</select>
									</div>
								</div>
								<div class="col-md-4 px-1">
									<div class="form-group">
										<label class="control-label">
											First Name
										</label>
										<input class="form-control" type="text" name="first_name" placeholder="ex: Mike">
									</div>
								</div>
								<div class="col-md-4 pl-1">
									<div class="form-group">
									<label class="control-label">
										Last Name
									</label>
									<input class="form-control" type="text" name="first_name" placeholder="ex: Mike">
								</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 pr-1">
									<div class="form-group">
										<label class="control-label">
											User Name
										</label>
										<input class="form-control" type="text" name="last_name" required="true" placeholder="ex: Andrew">
									</div>
								</div>
								<div class="col-md-4 px-1">
									<div class="form-group">
										<label class="control-label">
											Password
										</label>
										<input class="form-control" type="text" name="last_name" required="true" placeholder="ex: Andrew">
									</div>
								</div>
								<div class="col-md-4 pl-1">
									<div class="form-group">
										<label class="control-label">
											Confirm Password
										</label>
										<input class="form-control" type="text" name="last_name" required="true" placeholder="ex: Andrew">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label class="control-label">
											Your Email<star>*</star>
										</label>
										<input class="form-control" type="text" name="email" email="true" placeholder="ex: hello@creative-tim.com">
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label class="control-label">
											Your Email<star>*</star>
										</label>
										<input class="form-control" type="text" name="email" email="true" placeholder="ex: hello@creative-tim.com">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label for="inputState">Membership type</label>
										<select id="inputState" class="form-control">
											<option selected>Trial</option>
											<option>1 bulan</option>
											<option>3 bulan</option>
											<option>6 bulan</option>
											<option>12 bulan</option>
										</select>
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<div class="checkbox">
											<input id="checkbox12" type="checkbox">
											<label for="checkbox12">
												Yes, I have read and agree with <a>terms and condition</a>
											</label>
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="row mb-4">
								<div class="update ml-auto mr-auto">
									<button type="submit" class="btn btn-info btn-round btn-md">Sign up</button>
								</div>
							</div>
						</form>
				</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-header text-center">
					<h4 class="card-title">
						Sign Up with
					</h4>
				</div>
				<div class="card-content" style="padding:20px">
					<div class="row">
						<div class="col-md-10 col-md-offset-1" style="padding-bottom:10px;margin:0 auto;">
							<button class="btn btn-block btn-fill btn-google">
								<i class="fa fa-google"></i>
								Google
							</button>
						</div>
						<div class="col-md-10 col-md-offset-1" style="padding-top:10px;margin:0 auto;">
							<button class="btn btn-block  btn-fill">
								<i class="fa fa-facebook"> </i>
								facebook
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

<script>
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

<style type="text/css">
body{
	background-color: white;
}
.card-header {
	background-color: white !important;
	border: 0 !important;
}
.register-container {
    width: 550px;
    margin: auto;
}
.form-group {
    margin: 0;
    padding: 0;
    margin: 0 auto;
}
.btn-login{
    height: 50px;
    padding-left: 2.5rem;
    padding-right: 2.5rem;
}
.card-signup{
	padding: 50px;
	border: 0;
  	border-radius: 1rem;
  	box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
}
.btn-google{
	font-size: 80%;
    border-radius: 5rem !important;
    font-weight: bold;
    padding: 1rem;
    transition: all 0.2s;
}
.no-border{
	border: none !important;
}
.btn-primary {
    color: #fff;
    background-color: #3490dc;
    border-color: #3490dc;
}
.btn-google {
    color: #fff;
    background-color: #dc4e41;
    border-color: #dc4e41;
    color: #ffffff;
    font-size: 16px;
}
.btn-brand {
    color: #fff;
    background-color: #3d94fb;
    border-color: #3d94fb;
    color: #ffffff;
}
.btn-pill {
    border-radius: 50px!important;
}
</style>