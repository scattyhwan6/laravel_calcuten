@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto !important;z-index:1000;">

<nav class="navbar navbar-expand-md bg-grey" style="padding-top:0px;">
    <div class="container">
        <a class="navbar-brand text-white">File Folder - Sumber daya air</a>
            
    </div>
</nav>

<div class="container" style="padding-top:25px">
    <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7"></div>
        <div class="col-lg-6 col-5 text-right">
            <div style="width: 60%;margin: 0;float: left;">
                <form style="margin-top:20px">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control mr-sm-2 no-border" placeholder="Search...">
                        <button class="btn btn-primary btn-just-icon btn-round" type="submit">
                            <i aria-hidden="true" class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
            </div>
            <div class="mt-6 py-5" style="float:left">
                <a href="#" class="btn btn-link">Back</a>
            </div> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div id="table-scroll">
                    <section class="citakarya_list">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item info">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right  text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item info">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right  text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item info">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right  text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item info">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right  text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item info">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right  text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subsumberdayaair"> 
                                    <small class="pull-right text-white">15 April</small> 
                                    <strong>sumber daya air</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    
        {{-- <div class="card card-circle-chart" data-background-color="white">
                <div class="card-header text-center">
                    <h3 class="card-title">User Note:</h3>
                </div>
                <div class="card-content">
                    <ol class="text-left">
                        <li><p class="description">Klik analisa pekerjaan yang akan anda kerjakan.</p></li>
                        <li><p class="description">Bagi member trial hanya akan dapat menggunakan 10 analisa satuan teratas, selama masa trial</p></li>
                    </ol>
                </div>
            </div> --}}
        
    </div>




@endsection

<style>

#table-scroll{
    overflow:scroll !important;
    max-height:500px !important;
}
.list-group-item {
    background-color:#b3adad !important;
}
.list-group-item span{ 
    color:white;
}
.bg-green {
    background-color: #6dd3d6!important;
}

</style>