@extends('layouts.app')


@section('content')
<form id="allInputsFormValidation" class="form-horizontal" action="" method="post" novalidate="novalidate">
@method('PUT')
		                            <div class="card-content">
		                                <h4 class="card-title">Type Validation</h4>
		                                <fieldset>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">
													Required Text
												</label>
		                                        <div class="col-sm-6">
		                                            <input class="form-control error" type="text" name="required" required="required" aria-invalid="true"><label id="required-error" class="error" for="required">This field is required.</label>
		                                        </div>
		                                        <div class="col-sm-4"><code>required</code></div>
		                                    </div>
		                                </fieldset>

		                                <fieldset>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">
													Email
												</label>
		                                        <div class="col-sm-6">
		                                            <input class="form-control valid" type="text" name="email" email="true">
		                                        </div>
		                                        <div class="col-sm-4"><code>email="true"</code></div>
		                                    </div>
		                                </fieldset>

		                                <fieldset>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">
													Number
												</label>
		                                        <div class="col-sm-6">
		                                            <input class="form-control valid" type="text" name="number" number="true">
		                                        </div>
		                                        <div class="col-sm-4"><code>number="true"</code></div>
		                                    </div>
		                                </fieldset>

		                                <fieldset>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">
													Url
												</label>
		                                        <div class="col-sm-6">
		                                            <input class="form-control valid" type="text" name="url" url="true">
		                                        </div>
		                                        <div class="col-sm-4"><code>url="true"</code></div>
		                                    </div>
		                                </fieldset>

		                                <fieldset>
		                                    <div class="form-group column-sizing">
		                                    	<label class="col-sm-2 control-label">
													Equal to
												</label>
	                                    		<div class="col-sm-3">
	                                				<input class="form-control validate-equalTo-blur" id="idSource" type="text" placeholder="#idSource">
	                                    		</div>
		                                    	<div class="col-sm-3">
		                                        	<input class="form-control valid" id="idDestination" type="text" placeholder="#idDestination" equalto="#idSource">
		                                		</div>
		                                		<div class="col-sm-4"><code>equalTo="#idSource"</code>
		                                    	</div>
		                                	</div>
		                                </fieldset>

		                                <h4 class="card-title">Range validation</h4>

		                                <fieldset>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">
													Min Length
												</label>
		                                        <div class="col-sm-6">
		                                            <input class="form-control valid" type="text" name="min_length" minlength="5">
		                                        </div>
		                                        <div class="col-sm-4"><code>minLength="5"</code></div>
		                                    </div>
		                                </fieldset>

		                                <fieldset>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">
													Max Length
												</label>
		                                        <div class="col-sm-6">
		                                            <input class="form-control valid" type="text" name="max_length" maxlength="5">
		                                        </div>
		                                        <div class="col-sm-4"><code>maxLength="5"</code></div>
		                                    </div>
		                                </fieldset>

		                                <fieldset>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">Range</label>
		                                        <div class="col-sm-6">
		                                            <input class="form-control valid" type="text" name="range" range="[6,10]">
		                                        </div>
		                                        <div class="col-sm-4"><code>range="[6,10]"</code></div>
		                                    </div>
		                                </fieldset>

		                                <fieldset>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">
													Min Value
												</label>
		                                        <div class="col-sm-6">
		                                            <input class="form-control valid" type="text" name="min" min="6">
		                                        </div>
		                                        <div class="col-sm-4"><code>min="6"</code></div>
		                                    </div>
		                                </fieldset>

		                                <fieldset>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">
													Max Value
												</label>
		                                        <div class="col-sm-6">
		                                            <input class="form-control valid" type="text" name="max" max="6">
		                                        </div>
		                                        <div class="col-sm-4"><code>max="6"</code></div>
		                                    </div>
		                                </fieldset>
		                            </div>
									<div class="card-footer text-center">
										<button type="submit" class="btn btn-info btn-fill">Validate Inputs</button>
									</div>
		  						</form>

@endsection