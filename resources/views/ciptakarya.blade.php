@extends('layouts.app')


@section('content')
<nav class="navbar navbar-expand-md bg-primary" style="padding-top:0px;">
    <div class="container">
        <a class="navbar-brand">Cipta Karya</a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                     <li class="nav-item">
                       <form style="margin-top:20px">
                            <div class="input-group no-border">
                                <input type="text" class="form-control mr-sm-2 no-border" placeholder="Search...">
                                <button class="btn btn-primary btn-just-icon btn-round" type="submit">
                                    <i aria-hidden="true" class="fa fa-search"></i>
                                </button>
                            </div>
                        </form>
                    </li>
                    <li class="nav-item">
                        <a  class="nav-link" href="#!"> Folder </a>
                    </li>
                    <li class="nav-item">
                        <a  class="nav-link" href="#!"> Back </a>
                    </li>
                </ul>
            </div>
    </div>
</nav>

<div class="container py-5">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div id="table-scroll">
                    <section class="citakarya_list">
                        <ul class="list-group ">
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemnuatan Stripping </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemnuatan Stripping </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span class="txt-white">1 m2 Pemasangan Batu Pondasi </span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemnuatan Stripping </span> 
                                    <span class="label label-sm btn-success">New</span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a class="" href="subciptakarya"> 
                                    <small class="pull-right">15 April</small> 
                                    <strong>Cipta karya</strong>  
                                    <span>1 m2 Pemasangan Batu Pondasi </span> 
                                </a>
                            </li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-circle-chart" data-background-color="white">
                <div class="card-header text-center">
                    <h3 class="card-title">User Note:</h3>
                </div>
                <div class="card-content">
                    <ol class="text-left">
                        <li><p class="description">Klik analisa pekerjaan yang akan anda kerjakan.</p></li>
                        <li><p class="description">Bagi member trial hanya akan dapat menggunakan 10 analisa satuan teratas, selama masa trial</p></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<style>
.citakarya_list {
    border-spacing: 0;
    display: table;
    table-layout: fixed;
    width: 100%;
    height: 100%;
}
.bg-grey {
    background-color: #949494!important;
}
#table-scroll{
    overflow:scroll !important;
    max-height:700px !important;
}

.list-group-item {
    background-color: #afafaf !important;
    color: white !important;
    border: 1px solid #f5f5f5;
}
.list-group-item span {
    color:white;
}
</style>