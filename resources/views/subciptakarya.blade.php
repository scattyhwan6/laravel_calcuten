@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto !important;z-index:1000;background: #f4f3ef;">
    <nav class="navbar navbar-expand-md bg-info" style="padding-top:0px;">
        <div class="container">
            <a class="navbar-brand" style="color:teal">Cipta Karya</a>
        </div>
    </nav>
    <div class="container py-5">
        <div class="row align-items-center py-4">
                <div class="col-md-6">
                    <div class="title">
                        <h3 class="text-muted">Cipta Karya - 1m2 Pemasangan Batu Pondasi</h3>
                    </div>
                </div>
                <div class="col-md-6 text-right ">
                    <a href="#" class="btn btn-sm btn-link">Folder</a>
                    <a href="#" class="btn btn-sm btn-link">Back</a>
                    <a href="#" class="btn btn-sm btn-link">Home</a>
                </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="table-responsive table-hover">
                    @include('inc.ciptakaryatable')
                </div>
                    
            </div>
            <div class="col-md-4">
                <div class="card card-circle-chart" data-background-color="white" >
                    <div class="card-header text-center">
                        <h3 class="card-title">User Note :</h3>
                    </div>
                    <div class="card-content">
                        <ol class="text-left">
                            <li>
                                <p class="description">
                                    Koefisien menggunakan standart SNI dan 
                                    peraturan kementrian PUMR RI
                                </p>
                            </li>
                            <li>
                                <p class="description">
                                    Input satuan harga di row 5.
                                </p>
                            </li>
                            <li>
                                <p class="description">
                                    Rekomendasi satuan harga bisa didapatkan atau buku
                                    jurnal harga satuan bahan bangunan konstruksi dan interior
                                </p>
                            </li>
                        </ol>
                        
                    </div>
                </div>
                <div class="ml-auto mr-auto text-center">
                    <div class="links">
                        <ul class="uppercase-links">
                            <li>
                                <a href="">
                                    <button class="btn btn-info btn-just-icon" type="button">
                                        <i class="fa fa-file"></i>
                                    </button><br>
                                    New Project
                                </a>
                            </li>
                            <li>
                                <a href="#!" class="text-center" data-toggle="modal" data-target="#ShareModal">
                                    <button class="btn btn-info btn-just-icon" type="button" >
                                        <i class="fa fa-share-square"></i>
                                    </button><br>
                                    Share
                                </a>
                            </li>
                            <li>
                                <a href="#!" class="text-center" data-toggle="modal" data-target="#exampleModal">
                                    <button class="btn btn-info btn-just-icon" type="button">
                                        <i class="fa fa-save"></i>
                                    </button><br>
                                    save
                                </a>
                            </li>
                            <li>
                                <a href="" class="text-center">
                                    <button class="btn btn-info btn-just-icon" type="button">
                                        <i class="fa fa-print"></i>
                                    </button><br>
                                    Print
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<section>
    <div class="modal fade show" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="modal-save" aria-modal="true" style="display:none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Nama Proyek</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" value="cipta karya" id="example-text-input"><br>
                                <button class="btn">choose</button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Bidang Pekerjaan / Folder</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" value="cipta karya" id="example-text-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Sub Bidang Pekerjaan</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" value="1 m2 Stage Perencah Bambu" id="example-text-input">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Sharemodal --}}
    <div class="modal fade" id="ShareModal" tabindex="-1" role="dialog" aria-labelledby="ShareModal" aria-hidden="true" style="display:none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Share</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <a class="rounded-circle mb-5" href="" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                            <img src="../images/download.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/forms.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/whatsapp (1).png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/docs.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/letter.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/outlook.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/google-plus (1).png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/yahoo (1).png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/gmail.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/messenger (1).png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img src="../images/google-drive.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/google-drive.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/bluetooth.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img src="../images/one-drive.png" width="50">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<script>
function myFunction() {
  window.print();
}
</script>

<style>
.modal-backdrop {
    background-color: #68b3c873 !important;
}
.table-responsive {
    overflow:scroll;
    max-height:400px;
}
#table-scroll{
    overflow:scroll !important;
    max-height:600px !important;
}
.btn-round {
    width: 65px !important;
     height: 65px !important;
     padding: 10px;

}
.btn-round img {
    padding: 10px
}

</style>

