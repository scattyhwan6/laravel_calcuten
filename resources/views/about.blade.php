@extends('layouts.app')

@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto!important;z-index:1000;display:block;background:#b3aba8;">
    <nav class="navbar navbar-expand-md bg-primary">
        <div class="container">
            <a class="navbar-brand text-white">About Calculaten Apps</a>
        </div>
    </nav>
    <div class="container py-5 mb-6">
        <div class="row align-items-center">
            <div class="col-lg-6 col-7"></div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-link">Back</a>
            </div>
        </div>
        <div class="row" style="margin-top:5%;">
            <div class="col-md-6">
                <div class="accordion text-left" id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingTentangCalculaten">
                            <h5 class="mb-0">
                                <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#TentangCalculaten" aria-expanded="true" aria-controls="collapseOne">
                                Tentang Calculaten
                                </a>
                            </h5>
                        </div>
                        <div id="TentangCalculaten" class="collapse" aria-labelledby="headingTentangCalculaten" data-parent="#accordion">
                            <div class="card-body">
                                Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingMenjadiMember">
                            <h5 class="mb-0">
                                <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#MenjadiMember" aria-expanded="false" aria-controls="collapseTwo">
                                Bagaimana Menjadi Member
                                </a>
                            </h5>
                        </div>
                        <div id="MenjadiMember" class="collapse" aria-labelledby="headingMenjadiMember" data-parent="#accordion">
                            <div class="card-body">
                                Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingAplikasiCalculaten">
                            <h5 class="mb-0">
                                <a class="btn btn-link collapsed"  data-toggle="collapse" data-target="#AplikasiCalculaten" aria-expanded="false" aria-controls="collapseThree">
                                Bagaimana Cara Menggunakan Aplikasi Calculaten
                                </a>
                            </h5>
                        </div>
                        <div id="AplikasiCalculaten" class="collapse" aria-labelledby="headingAplikasiCalculaten" data-parent="#accordion">
                            <div class="card-body">
                                Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingKeluhan">
                            <h5 class="mb-0">
                                <a class="btn btn-link collapsed"  data-toggle="collapse" data-target="#Keluhan" aria-expanded="false" aria-controls="collapseThree">
                                Jika Memiliki Keluhan
                                </a>
                            </h5>
                        </div>
                        <div id="Keluhan" class="collapse" aria-labelledby="headingKeluhan" data-parent="#accordion">
                            <div class="card-body">
                                Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingKeluhan">
                            <h5 class="mb-0">
                                <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Kode standart nasional indonesia di calculaten
                                </a>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingKetentuan">
                            <h5 class="mb-0">
                                <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#Ketentuan" aria-expanded="false" aria-controls="collapseThree">
                                Syarat dan Ketentuan Menggunakan Aplikasi Calculaten
                                </a>
                            </h5>
                        </div>
                        <div id="Ketentuan" class="collapse" aria-labelledby="headingKetentuan" data-parent="#accordion">
                            <div class="card-body">
                                Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="accordion" id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingSyarat">
                            <h5 class="mb-0">
                                <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#Syarat" aria-expanded="true" aria-controls="collapseOne">
                                Syarat dan Ketentuan Menjadi Member Calculaten
                                </a>
                            </h5>
                        </div>
                        <div id="Syarat" class="collapse" aria-labelledby="headingSyarat" data-parent="#accordion">
                            <div class="card-body">
                                Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSumberDayaAir">
                        <h5 class="mb-0">
                            <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#SumberDayaAir" aria-expanded="false" aria-controls="collapseTwo">
                            Fitur Sumber Daya Air
                            </a>
                        </h5>
                        </div>
                        <div id="SumberDayaAir" class="collapse" aria-labelledby="headingSumberDayaAir" data-parent="#accordion">
                        <div class="card-body">
                            Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingCiptaKarya">
                        <h5 class="mb-0">
                            <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#CiptaKarya" aria-expanded="false" aria-controls="collapseThree">
                            Fitur Cipta Karya
                            </a>
                        </h5>
                        </div>
                        <div id="CiptaKarya" class="collapse" aria-labelledby="headingCiptaKarya" data-parent="#accordion">
                        <div class="card-body">
                            Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingKalkulasiPekerjaan">
                        <h5 class="mb-0">
                            <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#KalkulasiPekerjaan" aria-expanded="false" aria-controls="collapseThree">
                            Fitur Cipta Kalkulasi Pekerjaan
                            </a>
                        </h5>
                        </div>
                        <div id="KalkulasiPekerjaan" class="collapse" aria-labelledby="headingKalkulasiPekerjaan" data-parent="#accordion">
                        <div class="card-body">
                            Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFileFolder">
                        <h5 class="mb-0">
                            <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#FileFolder" aria-expanded="false" aria-controls="collapseFileFolder">
                            Fitur File Folder
                            </a>
                        </h5>
                        </div>
                        <div id="FileFolder" class="collapse" aria-labelledby="headingFileFolder" data-parent="#accordion">
                        <div class="card-body">
                            Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo.
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

<style>
.pb-6, .py-6 {
    padding-bottom: 4.5rem!important;
}
.mt--6, .my--6 {
    margin-top: -4.5rem!important;
}
.bg-default {
    color: #fff !important;
    border-color: #3c4d69;
    background-color: #3c4d69 !important;
}
.card{
    border-radius: 12px !important;
    box-shadow: 0 6px 10px -4px rgba(0,0,0,.15) !important;
}
.bg-header-default {
    background: linear-gradient(87deg,#172b4d 0,#1a174d 100%)!important;
}
.opacity-8 {
    opacity: .9!important;
}
.mask {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    transition: all .15s ease;
}

</style>