@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto!important;background:#fbfbfb;">
        <nav class="navbar navbar-expand-md bg-primary">
            <div class="container">
                <a class="navbar-brand" style="color:teal">File Folder - Sumber daya air - Cth Turap Banjir Kanal Timur</a>
            </div>
        </nav>
        <div class="container"style="padding:20px" >
            <div class="row align-items-center py-4">
                <div class="col-md-6">
                    <div class="title">
                        <h3>1m2 Pemasangan Batu Pondasi</h3>
                    </div>
                </div>
                <div class="col-md-6 text-right ">
                    <a href="#" class="btn btn-sm btn-link">Folder</a>
                    <a href="#" class="btn btn-sm btn-link">Back</a>
                    <a href="#" class="btn btn-sm btn-link">Home</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8" >
                    <div class="table-responsive table-stripe" id="sumberdaya_table">
                            @include('table.sumberdayaairtable')
                    </div>
                </div>
                <div id="editor"></div>
                <div class="col-md-4">
                    {{-- <div class="card card-circle-chart" data-background-color="white" >
                        <div class="card-header text-center">
                            <h3 class="card-title">User Note :</h3>
                        </div>
                        <div class="card-content">
                            <ol class="text-left">
                                <li>
                                    <p class="description">
                                        Koefisien menggunakan standart SNI dan 
                                        peraturan kementrian PUMR RI
                                    </p>
                                </li>
                                <li>
                                    <p class="description">
                                        Input satuan harga di row 5.
                                    </p>
                                </li>
                                <li>
                                    <p class="description">
                                        Rekomendasi satuan harga bisa didapatkan atau buku
                                        jurnal harga satuan bahan bangunan konstruksi dan interior
                                    </p>
                                </li>
                            </ol>
                            
                        </div>
                    </div> --}}
                    @include('inc.buttons')
                </div>
            </div>
        </div>
  
@endsection


