@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto!important;background:#151f20">
        <nav class="navbar navbar-expand-md bg-default">
            <div class="container">
                <a class="navbar-brand text-white">Membership</a>
            </div>
        </nav>
<div class="container  py-5">
    <div class="row">
       
        <div class="sonar-wrapper" style="display:fixed;">
            <a href="#!">
                <img src="../images/trial.png" width="200">
            </a>
        
            <div class="sonar-wave"></div>
        </div>
       
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">        
                <div class="card card-pricing" data-color="white">
                <h4 class="card-category text-center card-title text-success">1 Bulan</h4>
                    <div class="card-body">
                        <div class="price_table_row">
                            <h4><b>Promo</b> <span> Rp.12345678</span></h4>
                            <h2><strong>Rp. 1238929</strong></h2>
                        </div>
                        <ul>
                            <li>
                            1 bulan full access kalkulasi analisa satuan pekerjaan cipta karya
                            </li>
                            <li>1 bulan full access kalkulasi analisa satuan pekerjaan sumber daya air
                            </li>
                            <li>1 bulan full access kalkulasi proyek</li>
                            <li>Download hasil analisa</li>
                            <li>Share hasil analisa</li>
                        </ul>
                        <div class="card-footer text-center">
                            <a class="btn btn-success btn-round" href="pembayaran">Pick this plan</a>   
                        </div>                      
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">        
                <div class="card card-pricing" data-color="white">
                <h4 class="card-category text-center card-title text-grey">3 Bulan</h4>
                    <div class="card-body">
                        <div class="price_table_row">
                            <h4><b>Promo</b> <span> Rp.12345678</span></h4>
                            <h2 class="text-warning"><strong>Rp. 1238929</strong></h2>
                        </div>
                        <ul>
                            <li>
                            3 bulan full access kalkulasi analisa satuan pekerjaan cipta karya
                            </li>
                            <li>3 bulan full access kalkulasi analisa satuan pekerjaan sumber daya air
                            </li>
                            <li>3 bulan full access kalkulasi proyek</li>
                            <li>Download hasil analisa</li>
                            <li>Share hasil analisa</li>
                        </ul>
                        <div class="card-footer text-center">
                            <a class="btn btn-default btn-round" href="pembayaran">Pick this plan</a>   
                        </div>
                                                
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">        
                <div class="card card-pricing" data-color="white">
                <h4 class="card-category text-center card-title text-warning">6 Bulan</h4>
                    <div class="card-body">
                        <div class="price_table_row">
                            <h4><b>Promo</b> <span> Rp.12345678</span></h4>
                            <h2><strong>Rp. 1238929</strong></h2>
                        </div>
                        <ul>
                            <li>
                            1 bulan full access kalkulasi analisa satuan pekerjaan cipta karya
                            </li>
                            <li>1 bulan full access kalkulasi analisa satuan pekerjaan sumber daya air
                            </li>
                            <li>1 bulan full access kalkulasi proyek</li>
                            <li>Download hasil analisa</li>
                            <li>Share hasil analisa</li>
                        </ul>
                        <div class="card-footer text-center">
                            <a class="btn btn-warning btn-round" href="pembayaran">Pick this plan</a>   
                        </div>                         
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">        
                <div class="card card-pricing" data-color="white">
                <h4 class="card-category text-center card-title text-info">12 Bulan</h4>
                    <div class="card-body">
                        <div class="price_table_row">
                            <h4><b>Promo</b> <span> Rp.12345678</span></h4>
                            <h2><strong>Rp. 1238929</strong></h2>
                        </div>
                        <ul>
                            <li>
                            1 bulan full access kalkulasi analisa satuan pekerjaan cipta karya
                            </li>
                            <li>1 bulan full access kalkulasi analisa satuan pekerjaan sumber daya air
                            </li>
                            <li>1 bulan full access kalkulasi proyek</li>
                            <li>Download hasil analisa</li>
                            <li>Share hasil analisa</li>
                        </ul>
                        <div class="card-footer text-center">
                            <a class="btn btn-info btn-round" href="pembayaran">Pick this plan</a>   
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</div> 
   
@endsection

<style>
.section-gray {
    background-color: #d8d8d8;
}

.card-gradient {
    box-shadow:0 6px 20px 0 rgba(244, 143, 177, .5) !important;
}
.warning-bg {
    background:linear-gradient(45deg, #ff6f00, #ffca28) !important;
}
.info-bg {
    background:linear-gradient(45deg, #0288d1, #26c6da) !important;
}
.grey-bg{
    background:linear-gradient(45deg, #423a3a, #b7aeb1) !important;
}
.green-teal {
    background:linear-gradient(45deg, #43a047, #1de9b6) !important;
}
.green-heading{
    border-top:1px solid rgba(160, 160, 160, .2);
}
.txt-white {
    color:white;
}
.price_table_heading{
	font-size:1.64rem;
	line-height:110%;
    padding:10px;
	background:#EEE;
}

.cost{
	padding:30px;
	font-size:30px;
}
.cost span{
	font-size:15px;
}

.price_table_row:nth-of-type(even) {
    background: #F8F8F8;
}
.btn{
	border-radius:0px;
}

.sonar-wrapper {
  position: fixed;
  z-index: 10000;
  overflow: show;
  padding: 0 !important;
  left:10px;
}

/* the 'wave', same shape and size as its parent */
.sonar-wave {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background-color: HSL(45,100%,50%);
  opacity: 0;
  z-index: -1;
  pointer-events: none;
}
.btn-wd{
    border: 1px solid transparent;
    padding: 1.375rem 1.75rem !important;
    border-radius: 100% !important;
}
/*
  Animate!
  NOTE: add browser prefixes where needed.
*/
.sonar-wave {
  animation: sonarWave 2s linear infinite;
}

@keyframes sonarWave {
  from {
    opacity: 0.4;
  }
  to {
    transform: scale(3);
    opacity: 0;
  }
}


</style>