@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto !important;z-index:5000;background:#c1bbbb">
	 <nav  class="navbar navbar-expand-md bg-grey" style="padding:0px;width:100%;">
        <div class="container">
            <a class="navbar-brand text-white">Homepage</a>
		</div>
		<ul>
			<li>
				
			</li>
		</ul>
    </nav>
	<div class="container" style="padding-top:20px">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card text-center">
					<div class="card-header ">
						<h5 class="">Cipta Karya</h5>
					</div>
					<div class="card-body">
						<div class="text-center ">
							<img src="images/graphic-design.png" width="100">
						</div>
					</div>
					<div class="card-footer ">
					<div class="stats">
						<a href="ciptakarya" class="btn btn-info btn-link"> see detail</a>
					</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card text-center">
					<div class="card-header ">
						<h5 class="">Sumber daya air</h5>
					</div>
					<div class="card-body">
						<div class="text-center">
							<img src="images/drop.png" width="100">
						</div>
					</div>
					<div class="card-footer ">
					<div class="stats">
						<a href="filesumberdayaair" class="btn btn-info btn-link">see detail</a>
					</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card text-center">
					<div class="card-header ">
						<h5 class="">Kalkulasi</h5>
					</div>
					<div class="card-body">
						<div class="text-center">
							<img src="images/budget.png" width="100">
						</div>
					</div>
					<div class="card-footer">
						<div class="stats">
							<a href="kalkulasi" class="btn btn-info btn-link">see detail</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card text-center">
					<div class="card-header ">
						<h5 class="">File Folder</h5>
					</div>
					<div class="card-body">
						<div class="text-center">
							<img src="images/folder.png" width="100">
						</div>
					</div>
					<div class="card-footer ">
					<div class="stats">
						<a href="filefolder" class="btn btn-default btn-link">see detail</a>
					</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card text-center">
					<div class="card-header ">
						<h5 class="">Membership</h5>
					</div>
					<div class="card-body">
						<div class="text-center">
							<img src="images/friendship.png" width="100">
						</div>
					</div>
					<div class="card-footer ">
					<div class="stats">
						<a href="membership" class="btn btn-default btn-link">see detail</a>
					</div>
					</div>
				</div>
			</div>
		</div>	
  		<div class="row">
			<div  class="col-md-8 mb-5">
				<h2  class="title">Calculaten</h2>
				<h5 class="description">Calculaten adalah aplikasi web untuk kalkulasi analisa pekerjaan konstruksi sesuai standart nasional indonesia</h5>
				<ol>
					<li>mohon register dan pilih paket</li>
					<li>anda juga dapat menggunakan program trial</li>
				</ol>
			</div>
		</div>
	</div>
@endsection

<style>
.card-header {
	border:none !important;
	background: transparent!important;
}
.card-footer {
	background: transparent !important;
}
.section-white-gradient {
	background: linear-gradient(0deg, #E5E5E5 0%, #fff 100%);
}
.card-footer {
	border: 0 !important;
}
</style>