

<link rel="stylesheet" type="text/css" href="/css/table-custom.css">
    <table class="tableizer-table table-hover">
        <thead>
            <tr class="tableizer-firstrow">
                <th>
                    <div class="relative">
                        <span class="colHeader">Pekerjaan</span>
                    </div>
                </th>
                <th>
                    <div class="relative">
                        <span class="colHeader">Nilai Satuan</span>
                    </div>
                </th>
                <th>
                    <div class="relative">
                        <span class="colHeader">Sub</span>
                    </div>
                </th>
                <th>
                    <div class="relative">
                        <span class="colHeader">Total</span>
                    </div>
                </th>
                
            </tr>
            <tr class="text-center">
                <td>
                    <div class="relative"><span class="rowHeader">1</span></div>
                </td>
                <td>
                    <div class="relative"><span class="rowHeader">2</span></div>
                </td>
                <td>
                    <div class="relative"><span class="rowHeader">3</span></div>
                </td>
                <td>
                    <div class="relative"><span class="rowHeader">4</span></div>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr class="txtKal">
                <td contenteditable="true">1 m2 Pekerjaan Perencah Bambu</td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom nilaisat text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom subtxt text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom multTotal text-center" disabled style="">0.00</textarea>
                    </div>
                </td>
            </tr>
            <tr class="txtKal">
                <td contenteditable="true">1 m2 Urugan Tanah Biasa</td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom nilaisat text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom subtxt text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom multTotal text-center" disabled style="">0.00</textarea>
                    </div>
                </td>
            </tr>
            <tr class="txtKal">
                <td contenteditable="true">1 m2 Batu Pondasi</td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom nilaisat text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom subtxt text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom multTotal text-center" disabled style="">0.00</textarea>
                    </div>
                </td>
            </tr>
            <tr class="txtKal">
                <td contenteditable="true">1 m2 Plester Pondasi</td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom nilaisat text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom subtxt text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom multTotal text-center" disabled style="">0.00</textarea>
                    </div>
                </td>
            </tr>
            <tr class="txtKal">
                <td contenteditable="true">1 m2 Tiang Pondasi</td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom nilaisat text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom subtxt text-center"  style="">0.00</textarea>
                    </div>
                </td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea tabindex="-1" class="handsontableInput 
                        area-custom multTotal text-center" disabled style="">0.00</textarea>
                    </div>
                </td>
            </tr>
            <tr class="totalfooter">
                <td>
                    <span style="font-weight:bold">Total</span>
                </td>
                <td>&nbsp;</td>
                <td></td>
                <td align="right">
                    <div class="handsontableInputHolder">
                        <textarea id="grandTotal" tabindex="-1" class="handsontableInput 
                        area-custom text-center text-bold" disabled style="">0.00</textarea>
                    </div>
                </td>
            </tr>
        
            
        </tbody>
    </table> 
<script type="text/javascript" src="{{asset('js/jquery.min.js') }}"></script>

<script>

$(document).ready(function () {
    $(".txtKal,.txtKalkulasi input").keyup(multInputs);
    function multInputs() {
        var kal=0;
        //tenaga kerja
        $("tr.txtKal").each(function () {
            // get the values from this row:
            var $nilaisat = $('.nilaisat', this).val();
            var $subtxt = $('.subtxt', this).val();
            var $total = ($nilaisat * 1) * ($subtxt * 1)
            $('.multTotal', this).text($total);
            kal += $total;
        });
        $("#grandTotal").text(kal);
    }
});
</script>