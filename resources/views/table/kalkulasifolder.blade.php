<link rel="stylesheet" type="text/css" href="/css/table-custom.css">


<link rel="stylesheet" type="text/css" href="/css/table-custom.css">
<table class="tableizer-table table-stripe" id="kalkulasi-table">
    <thead>
        <tr class="tableizer-firstrow">
            <th>
                <div class="relative">
                    <span class="colHeader">Pekerjaan</span>
                </div>
            </th>
            <th>
                <div class="relative">
                    <span class="colHeader">Nilai Satuan</span>
                </div>
            </th>
            <th>
                <div class="relative">
                    <span class="colHeader">Sub</span>
                </div>
            </th>
             <th>
                <div class="relative">
                    <span class="colHeader">Total</span>
                </div>
            </th>
        </tr>
        <tr>
            <td>
                <div class="relative"><span class="rowHeader">1</span></div>
            </td>
            <td>
                <div class="relative"><span class="rowHeader">2</span></div>
            </td>
            <td>
                <div class="relative"><span class="rowHeader">3</span></div>
            </td>
            <td>
                <div class="relative"><span class="rowHeader">4</span></div>
            </td>
        </tr>
    </thead>
    <tbody>
        <tr id="Row2" class="txtKal">
            <td class="col-file">
                <input type="file" id="myFile">
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  nilaisat text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  subtxt text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  multTotal text-center text-bold" disabled style="">0.00</textarea>
                </div>
            </td>
        </tr>
        <tr id="Row2" class="txtKal">
            <td class="col-file">
                <input type="file" id="myFile">
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  nilaisat text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  subtxt text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  multTotal text-center text-bold" disabled style="">0.00</textarea>
                </div>
            </td>
        </tr>
        <tr id="Row2" class="txtKal">
            <td class="col-file">
                <input type="file" id="myFile">
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  nilaisat text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  subtxt text-center text-bold"  style="">0.00</textarea>
                </div>
            </td>
            <td>
                <div class="handsontableInputHolder">
                    <textarea tabindex="-1" class="handsontableInput 
                    area-custom  multTotal text-center text-bold" disabled style="">0.00</textarea>
                </div>
            </td>
        </tr>
        <tfoot>
            <tr>
                <td>
                    <button type="button" id="btnAdd" class="btn btn-default btn-link" >Add new column</button></br></br>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="sumValue">
                <td>
                    <h4 style="font-weight:bold;text-align:center;padding:10px">Total</h4>
                </td>
                <td></td>
                <td></td>
                <td>
                    <div class="handsontableInputHolder">
                        <textarea id="grandTotal" tabindex="-1" class="handsontableInput 
                        area-custom   text-center text-bold" disabled style="display:block">0.00</textarea>
                    </div>
                  
                </td>
            </tr>
        </tfoot>
    </tbody>
</table> 

<style>
td.col-file {
    line-height: 20px;
    padding: 10px;
    height: 50px !important;
}

</style>
<script type="text/javascript" src="{{asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/function-table.js') }}"></script> 
<script type="text/javascript" src="{{asset('js/jspdf.min.js') }}"></script>
<script>
 $(document).ready(function () {
    $(".txtKal,.txtKalkulasi input").keyup(multInputs);
    function multInputs() {
        var kal=0;
        
        //tenaga kerja
        $("tr.txtKal").each(function () {
            // get the values from this row:
            var $nilaisat = $('.nilaisat', this).val();
            var $subtxt = $('.subtxt', this).val();
            var $total = ($nilaisat * 1) * ($subtxt * 1)
            
            $('.multTotal', this).text($total);
            kal += $total;
        });
        $("#grandTotal").text(kal);
        
        // $("#grandTotal").text(kal);
    }
   
    
});
 
</script>