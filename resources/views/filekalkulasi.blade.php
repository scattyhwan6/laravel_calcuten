@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto!important;">
        <nav class="navbar navbar-expand-md bg-grey">
            <div class="container">
                <a class="navbar-brand text-white">File Folder / Kalkulasi Proyek / Cth Turap Banjir Kanal Timur</a>
            </div>
        </nav>
        <div class="container">
            <div class="row align-items-center py-4">
                    <div class="col-md-6"></div>
                    <div class="col-md-6 text-right ">
                        <a href="#" class="btn btn-sm btn-link">Back</a>
                        <a href="#" class="btn btn-sm btn-link">Home</a>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    @include('table.kalkulasi')
                </div>
                <div class="col-md-4 text-right">
                
                    @include('inc.buttons')
                </div>
            </div>
        </div>


@endsection