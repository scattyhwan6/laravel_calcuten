
@extends('layouts.app')


@section('content')
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:auto !important;z-index:1000;background: #4c4a4a;">
    <nav  class="navbar navbar-expand-md bg-danger" style="padding:0px;width:100%;">
        <div class="container">
            <a class="navbar-brand text-white">Group Request</a>

        </div>
    </nav>
    <div class="container py-5">
        <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
                <div class="title">
                    <h3 class="text-white">Request Group Quotation</h3>
                </div>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">Back</a>
              <a href="#" class="btn btn-sm btn-neutral">Home</a>
            </div>
        </div>
        <div class="col-md-8" style="">
            <div class="card">
                <div class="card-body">
                    <form>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Perusahaan</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="Type Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Penanggung Jawab</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text"placeholder="Type Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Nomer Handphone</label>
                                <div class="col-md-8 form-inline">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" placeholder="type here" >
                                        </div>
                                        <div class="col-md-4 form-inline">
                                            <div class="form-group">
                                                <label class="form-control-label" style="margin-right:10px">nomer <br>alternative</label>
                                                <input type="text" class="form-control" placeholder="type here" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Email</label>
                            <div class="col-md-8">
                                <input class="form-control" type="email" placeholder="Type Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Alamat</label>
                            <div class="col-md-8">
                                <textarea class="form-control" placeholder="Type Here" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Jumlah Group</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" placeholder="Type Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Jumlah Group</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="custom-control custom-checkbox mb-3">
                                            <input class="custom-control-input" id="starter" type="checkbox">
                                            <label class="custom-control-label" for="starter">Starter</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="custom-control custom-checkbox mb-3">
                                            <input class="custom-control-input" id="Business" type="checkbox">
                                            <label class="custom-control-label" for="Business">Business</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="custom-control custom-checkbox mb-3">
                                            <input class="custom-control-input" id="Enterprise" type="checkbox">
                                            <label class="custom-control-label" for="Enterprise">Enterprise</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Quotation</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox mb-3">
                                            <input class="custom-control-input" id="same-email" type="checkbox">
                                            <label class="custom-control-label" for="same-email">Dikirim sesuai email</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox mb-3">
                                            <input class="custom-control-input" id="diff-email" type="checkbox">
                                            <label class="custom-control-label" for="diff-email">Email berbeda</label>
                                        </div>
                                    </div>
                                </div>
                                <input class="form-control" type="text" placeholder="Type Here">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlTextarea2">Catatan</label>
                            <textarea class="form-control" id="exampleFormControlTextarea2" rows="3" resize="none"></textarea>
                        </div>
                    </form>
                    <div class="pull-right">
                         <button  class="btn btn-default btn-round" type="button">Submit</button>
                    </div>
                   
                </div>
            </div>
        
        </div>
    </div>


@endsection