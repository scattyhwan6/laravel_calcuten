<section>
     {{-- save-modal --}}
    <div class="modal fade show " id="deletedModal" tabindex="-1" role="dialog" aria-labelledby="deletedModal" aria-modal="true" style="display:none;">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="no-title modal-header no-border-header">
                    <button class="close" type="button">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <h5>Are you sure you want to do this? </h5>
                </div><div class="modal-footer"><div class="left-side"><button class="btn btn-default btn-link" type="button">Never mind</button></div><div class="divider"></div><div class="right-side"><button class="btn btn-danger btn-link" type="button">Yes</button></div></div></div>
        </div>
    </div>
    {{-- save-modal --}}
    <div class="modal fade show" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="modal-save" aria-modal="true" style="display:none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Nama Proyek</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" value="cipta karya" id="example-text-input"><br>
                                <button class="btn">choose</button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Bidang Pekerjaan / Folder</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" value="cipta karya" id="example-text-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-4 col-form-label form-control-label">Sub Bidang Pekerjaan</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" value="1 m2 Stage Perencah Bambu" id="example-text-input">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Sharemodal --}}
    <div class="modal fade" id="ShareModal" tabindex="-1" role="dialog" aria-labelledby="ShareModal" aria-hidden="true" style="display:none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Share</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <a class="rounded-circle" href="" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                            <img class="mb-5" src="../images/download.png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img class="mb-5" src="../images/forms.png" width="50">
                        </a>
                        <a class="rounded-circle " href="#!">
                            <img class="mb-5" src="../images/whatsapp (1).png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img class="mb-5" src="../images/docs.png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img class="mb-5" src="../images/letter.png" width="50">
                        </a>
                        <a class="rounded-circle " href="#!">
                            <img class="mb-5" src="../images/outlook.png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img  class="mb-5" src="../images/google-plus (1).png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img  class="mb-5" src="../images/yahoo (1).png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img  class="mb-5" src="../images/gmail.png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img class="mb-5" src="../images/messenger (1).png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img  class="mb-5" src="../images/google-drive.png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img class="mb-5" src="../images/google-drive.png" width="50">
                        </a>
                        <a class="rounded-circle" href="#!">
                            <img class="mb-5" src="../images/bluetooth.png" width="50">
                        </a>
                        <a class="rounded-circle mb-5" href="#!">
                            <img class="mb-5" src="../images/one-drive.png" width="50">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
.modal-header.no-border-header {
    border-bottom: 0!important;
}
.modal-header {
    border-bottom: 1px solid #ddd;
    padding: 20px;
    text-align: center;
    display: block!important;
}
.modal-body {
    padding: 20px 50px;
    color: #000;
}
.modal-footer {
    border-top: 1px solid #ddd;
    padding: 0;
}
.modal-footer .left-side, .modal-footer .right-side {
    display: inline-block;
    text-align: center;
    width: 49%;
}
.modal.fade .modal-dialog {
    transition: transform .3s ease-out,-webkit-transform .3s ease-out;
    -webkit-transform: translate(0,-25%);
    transform: translate(0,-25%);
}
.modal-dialog {
    padding-top: 60px;
}
.modal-content {
    border: 0;
    border-radius: 10px;
    box-shadow: 0 0 15px rgba(0,0,0,.15), 0 0 1px 1px rgba(0,0,0,.1);
}
.modal-footer>:not(:last-child) {
    margin-right: .25rem;
}
.modal-footer .divider {
    background-color: #ddd;
    display: inline-block;
    float: inherit;
    height: 63px;
    margin: 0 -3px;
    width: 1px;
}
.modal-footer .btn-link {
    padding: 20px;
    width: 100%;
}
.btn-danger.btn-link {
    color: #f5593d !important;
}
</style>

<script>
var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

$('#cmd').click(function () {
    doc.fromHTML($('#content').html(), 15, 15, {
        'width': 170,
            'elementHandlers': specialElementHandlers
    });
    doc.save('sample-file.pdf');
});

</script>