<div class="card card-payment pull-right">
    <div class="card-body text-center" style="margin:0 auto;">
        <h5 class="card-title text-center">Please choose your prefer payment method</h5>
        <hr>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-mandiri-tab" data-toggle="tab" href="#nav-mandiri" role="tab" aria-controls="nav-mandiri" aria-selected="true">
                    <img src="../images/mandiri-logo.png" width="60">
                </a>
                <a class="nav-item nav-link" id="nav-bca-tab" data-toggle="tab" href="#nav-bca" role="tab" aria-controls="nav-bca" aria-selected="false">
                    <img src="../images/bca.png" width="60">
                </a>
                <a class="nav-item nav-link" id="nav-virtual-tab" data-toggle="tab" href="#nav-virtual" role="tab" aria-controls="nav-virtual" aria-selected="false">
                    <img src="../images/virtual-account.png" width="60">
                </a>
                    <a class="nav-item nav-link" id="nav-master-tab" data-toggle="tab" href="#nav-master" role="tab" aria-controls="nav-virtual" aria-selected="false">
                    <img src="../images/visa-master.png" width="60">
                </a>
                <a class="nav-item nav-link" id="nav-gopay-tab" data-toggle="tab" href="#nav-gopay" role="tab" aria-controls="nav-virtual" aria-selected="false">
                    <img src="../images/gopay.png" width="60">
                </a>
                <a class="nav-item nav-link" id="nav-paypal-tab" data-toggle="tab" href="#nav-paypal" role="tab" aria-controls="nav-virtual" aria-selected="false">
                    <img src="../images/paypal.png" width="60">
                </a>
                <a class="nav-item nav-link" id="nav-indomaret-tab" data-toggle="tab" href="#nav-indomaret" role="tab" aria-controls="nav-indomaret" aria-selected="false">
                    <span class="form-inline">
                        <img src="../images/indomaret.png" width="60">
                    <img src="../images/alfamart.png" width="60"></span>
                </a>
            </div>
        </nav>
        <hr>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active text-center" id="nav-mandiri" role="tabpanel" aria-labelledby="nav-mandiri">
                    <h5>Mandiri : 13700.21392.2920</h5>
                    <span>Atas nama</span><br>
                    <h5>PT.Calculaten Inovation Nusantara</h5>
                    <span>KCP Kwitang Jakarta</span>
                    <h5>Untuk aktivasi instan tuliskan berita transfer:</h5>
                    <span class="text-danger">201921022</span><br>
                    <button  class="btn btn-default mt-3" type="button">Paid</button>
            </div>
            <div class="tab-pane fade text-center" id="nav-bca" role="tabpanel" aria-labelledby="nav-profile-tab">
                <h5>BCA : 13700.21392.2920</h5>
                    <span>Atas nama</span><br>
                    <h5>PT.Calculaten Inovation Nusantara</h5>
                    <span>KCP Kwitang Jakarta</span>
                    <h5>Untuk aktivasi instan tuliskan berita transfer:</h5>
                    <span class="text-danger">201921022</span><br>
                    <button  class="btn btn-default mt-3" type="button">Paid</button>
            </div>
            <div class="tab-pane fade text-center" id="nav-virtual" role="tabpanel" aria-labelledby="nav-virtual-tab">
                <h5>BCA : 13700.21392.2920</h5>
                    <span>Atas nama</span><br>
                    <h5>PT.Calculaten Inovation Nusantara</h5>
                    <span>KCP Kwitang Jakarta</span>
                    <h5>Untuk aktivasi instan tuliskan berita transfer:</h5>
                    <span class="text-danger">201921022</span><br>
                    <button  class="btn btn-default mt-3" type="button">Paid</button>
            </div>
            <div class="tab-pane fade text-center" id="nav-master" role="tabpanel" aria-labelledby="nav-master-tab">
                <h5>Master - visa </h5><br>
                <button  class="btn btn-default mt-3" type="button">Paid</button>
            </div>
            <div class="tab-pane fade text-center" id="nav-gopay" role="tabpanel" aria-labelledby="nav-master-tab">
                <h5>Gopay</h5><br>
                <button  class="btn btn-default mt-3" type="button">Paid</button>
            </div>
            <div class="tab-pane fade text-center" id="nav-paypal" role="tabpanel" aria-labelledby="nav-master-tab">
                <h5>Paypal</h5><br>
                    <span>Atas nama</span><br>
                    <h5>PT.Calculaten Inovation Nusantara</h5>
                    <span>KCP Kwitang Jakarta</span>
                    <h5>Untuk aktivasi instan tuliskan berita transfer:</h5>
                    <span class="text-danger">201921022</span><br>
                    <button  class="btn btn-default mt-3" type="button">Paid</button>
            </div>
            <div class="tab-pane fade text-center" id="nav-indomaret" role="tabpanel" aria-labelledby="nav-master-tab">
                <h5>Indomaret - alfamart</h5><br>
                
                    <button  class="btn btn-default mt-3" type="button">Paid</button>
            </div>
        </div>
    </div>
</div>


<style>
.card-payment{
    width: 350px;
}
.nav-tabs{
    border:0 !important;
    padding: 0 !important;
    height: auto;
    margin: 0 !important;


}
.nav-tabs > .nav-link.active {
    color: #495057;
    background-color: #f8fafc; */
    border:0px !important;
}
.nav-link {
    border: 0 !important;
    border-top-left-radius: 0 !important;
    border-top-right-radius: 0 !important;
}
.nav-tabs a:hover{
    background: gray;

}
.card {
    border-radius: 12px !important;
    box-shadow: 0 6px 10px -4px rgba(0,0,0,.15);
    color: #333;
    margin-bottom: 20px;
    position: relative;
    z-index: 1;
    border: 0;
    transition: transform .3s cubic-bezier(.34,2,.6,1),box-shadow .2s,-webkit-transform .3s cubic-bezier(.34,2,.6,1);
}
</style>