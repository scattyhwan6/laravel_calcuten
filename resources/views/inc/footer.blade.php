
<footer class="footer section-black py-5">
        <div class="container">
            <div class="row">
                <nav class="footer-nav ml-auto mr-auto">
                    <ul>
                        <li><a href="about">About App</a></li>
                        <li>
                            <a href="grouprequest">Group Request</a>
                        </li>
                        <li>
                            <a href="changeprofile">Change Profile</a>
                        </li>
                        <li>
                            <a href="contactus">Contact Us</a>
                        </li>
                    </ul>
                </nav>
            </div>
      </footer>
<style>
.footer {
    background-attachment: fixed;
    position: relative;
    background-color: grey;
    line-height: 36px !important;
}
.footer nav>ul a:not(.btn) {
    color: #fff;
    display: block;
    margin-bottom: 3px;
    line-height: 1.6;
    font-size: 12px;
    text-transform: uppercase;
    font-weight: 600;
}

.copyright {
    color: #7a9e9f !important;
}
 .py-5 {
	 padding-bottom: 10px!important;
	 padding-top: 10px!important;
 }
 .section-black {
    background-color: rgb(51, 51, 51);
}
</style>