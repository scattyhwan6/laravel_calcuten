<header class="navbar navbar-horizontal navbar-expand navbar-dark bg-default flex-row align-items-md-center ct-navbar">
    <ul class="navbar-nav flex-row mr-auto  d-none d-md-flex" style="margin:0 auto !important;">
        <li class="nav-item">
            <a class="nav-link active" href="{{URL::to('admin/home')}}">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{URL::to('admin/sumberdayaair')}}" target="_blank">sumber daya air</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{URL::to('admin/ciptakarya')}}" target="_blank">cipta karya</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{URL::to('admin/userdatabase')}}" target="_blank">user database</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{URL::to('admin/membership')}}" target="_blank">user membership</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{URL::to('admin/createuser')}}" target="_blank">create user</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{URL::to('admin/newadmin')}}" target="_blank">new admin</a>
        </li>
    </ul>
</header>

<style>
.nav-link {
    font-size: 15px !important;
    text-transform:uppercase !important;
}
.ct-navbar {
    background-color:#8eb35d;
}

</style>