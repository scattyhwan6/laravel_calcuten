<div class="ml-auto mr-auto text-center" style="position:absolute;bottom:0">
    <div class="links">
        <ul class="uppercase-links">
            <li>
                <a href="#!" id="export"  class="text-grey" onclick="javascript:demoFromHTML();">
                    <img src="../images/download.png" width="40"><br>
                    Share
                </a>
            </li>
            <li>
                <a href="#!" class="text-white">
                    <a class="" href="#!" data-toggle="modal" data-target="#ShareModal">
                        <img src="../images/share copy.png" width="40">
                    </a><br>
                    Share
                </a>
            </li>
            <li>
                <a href="#!" class="text-white">
                    <a class="" href="" data-toggle="modal" data-target="#deletedModal">
                        <img src="../images/cancel.png" width=40>
                    </a><br>
                    Deleted
                </a>
            </li>
            <li>
                <a href="#!" class="btnPrint" onclick="javascript:printData();">
                    <img src="../images/printer.png" width="40"><br>
                    Print
                </a>
            </li>
        </ul>
    </div>
</div>

<script>
function printData() {
        window.print()
}
</script>