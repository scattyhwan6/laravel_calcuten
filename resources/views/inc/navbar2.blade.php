<nav class="navbar navbar-expand-lg">
    <div class="container">
        <a class="" href="home" style="z-index:1000">
            <img src="../images/logo3.png" width="500">
        </a>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav mr-auto" style="z-index:1000;">
                <li class="nav-item">
                    <a href="login" class="nav-link" style="color:dimgray">
                        <span class="font-weight-bold">Login</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="register" class="nav-link" style="color:dimgray">
                        <span class="font-weight-bold">Sign Up</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="profile-photo-small">
                                <img class="img-circle img-responsive img-no-padding" alt="Image placeholder" src="images/user.jpg">
                            </span>
                            <div class="media-body">
                                <span class="mb-0 user">John Snow</span>
                            </div>
                        </div>
                    </a>
                   <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    
                    <div class="dropdown-divider"></div>
                    <a href="#!" class="dropdown-item">
                    <i class="ni ni-user-run"></i>
                    <span>Logout</span>
                    </a>
                </div>
                    
                </li>
            </ul>
            
        </div>
    </div>
</nav>

<style>

.profile-photo-small {
    width: 40px;
    height: 30px;
    margin: -10px 0 0 -15px;
}
.img-circle {
    background-color: #fff;
    margin-bottom: 10px;
    padding: 4px;
    border-radius: 50%!important;
    max-width: 100%;
}
.nav-link {
    line-height: 1.6;
    margin: 15px 3px;
    padding: 10px 15px;
    opacity: .8;
    font-size: 12px;
    text-transform: uppercase;
    font-weight: 600;
    color: #66615b;
}
.user{
        opacity: .8;
    font-size: 12px;
    text-transform: uppercase;
    font-weight: 600;
    color: dimgray;
}
</style>